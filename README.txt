Linked Media Framework
Quick Installation Instructions
===============================

The Linked Media Framework (LMF) is implemented as a Java Web Application 
that can in principle be deployed to any Java Application Server. It has 
been tested under Tomcat 7.x. It can be installed using the source code or 
as a binary package.

Requirements
------------

Hardware:
- standard workstation (Dual-Core or similar)
- 1GB main memory
- about 100MB hard disk

Software:
- Java JDK 67 or higher
- Java Application Server compatible with Servlet 3.0 (Tomcat 7.x)
- Database (PostgreSQL, MySQL - if not explicitly configured, an embedded
  H2 database will be used)


Installation (Standalone)
-------------------------

The most straightforward way of installing the LMF is to use the standalone installer, 
which will automatically install and configure all core components of the LMF including 
a version of Apache Tomcat for running the web application.

To install the standalone version, download the lmf-installer-...-standalone.jar files 
from the downloads section (http://code.google.com/p/lmf/downloads/list) and execute 
the jar file (double click or java -jar command).

IMPORTANT: The installation path on windows systems may not contain whitespaces (e.g. 
'C:/Program Files' is not usable). This is a serious bug of Tomcat application server 
and will most probably be fixed in further versions.

Depending on the underlying operating system, the installer will also create shortcuts 
for starting/stopping the LMF server:

  * on Windows, these can be found in the Start Menu under "Salzburg NewMediaLab -> Linked Media Framework"
  * on Linux, these are created on the desktop and also in the Applications menu under "Linked Media Framework"
  * on MacOS, there are two actions "Start Linked Media Framework" and "Shutdown Linked Media Framework" in the program folder of the LMF installation

After installation, you can access the administration interface of the Linked Media 
Framework by pointing your browser to `http://{your_host_name}:8080/LMF` or by clicking 
on the LMF systray icon and selecting "Administration" from the menu.


Installation (Binary)
--------------------

The binary installation comes as a Java Web Archive (.war) file that can be deployed 
in any application server. The deployment procedure is as follows:


  # Download and install the application server (Tomcat 7.0.x or Jetty 6.x) and the database you intend to use (optional, default is H2 embedded)
  # Copy the .war file into the application server's deployment directory (Tomcat and Jetty: the webapps subdirectory)
  # In the console where you will start the application server, set the environment variable LMF_HOME to the directory that will be used for storing the persistent runtime data of the Linked Media Server
  # Startup the application server and go to the deployment URL with a web browser (e.g. http://localhost:8080). The Linked Media Server will then carry out initial setup using the embedded H2 database. The default interface will also contain links to the admin area and API documentation of the Linked Media Server.
  # (OPTIONAL) If you do not want to use H2, go to the admin interface and configure the database according to your own preferences. It is recommended that you restart the application server when you have done so.

To avoid setting the environment variable LMF_HOME every time you open
a new terminal, you can also enter it persistently by adding the following 
lines to catalina.sh (Unix):

  export LMF_HOME=<PATH-TO-HOME>
  export JAVA_OPTS="-Djava.net.preferIPv4Stack=true -Xmx1024m -XX:PermSize=128m -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled"

The latter option will give more memory to the Linked Media Server. You can even 
increase the value of 1024 to something higher as needed.

Specific Settings for Tomcat
----------------------------

In production environments, Apache Tomcat will be the application server of choice. 
This is a collection of issues arising with Tomcat installations.

We strongly recommend to use Tomcat 7.0.x, precisely we recommend to use 
Tomcat >= 7.0.35 to work with the LMF.

If you want to have multimple instance of LMF running on Tomcat there are some settings 
it might be desirable to set up multiple instances of the LMF in the same application 
server installation under different context URLs. This can be achieved by creating 
context definition files under conf/Catalina/localhost/<appname>.xml, where <appname>.xml 
is a context configuration file. `<appname>` is the name of the web application, e.g. 
"LMF" or "MyApp". The file will contain a configuration similar to the following:

  <Context docBase="/data/LMF/build/libs/LMF-2.0rc5-SNAPSHOT.war" unpackWAR="false" useNaming="true">
    <Parameter name="kiwi.home" value="/data/instances/lmf" override="false"/>
    <Resource name="BeanManager" auth="Container"
              type="javax.enterprise.inject.spi.BeanManager"
              factory="org.jboss.weld.resources.ManagerObjectFactory"
              />
  </Context>

  * The docBase attribute specifies the location of the WAR file of the LMF in case it is not located in the webapps directory. 
  * The value of the parameter kiwi.home provides the location of the LMF home directory (for historical reasons called kiwi.home)
  * The Resource registers a factory for creating the Java EE 6 Bean Manager. This entry will typically remain unchanged, but it is necessary for the system to work properly.


Installation (Maven)
--------------------

It is easy to build custom projects based on the LMF using a Maven archetype. To 
create a custom project, simply run the following command and enter the base 
information when prompted:

  mvn archetype:generate -DarchetypeArtifactId=lmf-archetype-webapp 
      -DarchetypeGroupId=at.newmedialab.lmf -DarchetypeVersion=2.6.0 
      -DarchetypeRepository=http://devel.kiwi-project.eu:8080/nexus/content/repositories/releases/

Afterwards, change to the newly created project directory and run:

  mvn clean tomcat7:run

To start an empty LMF installation.


Installation (Source)
---------------------

The LMF Source Code can be obtained from the [http://code.google.com/p/lmf/source/checkout 
Mercurial Repository] at Google Code. Please follow the instructions provided by Google Code.

Additional Requirements:
  * Mercurial (http://mercurial.selenic.com)
  * Maven 3.x (http://maven.apache.org)
  * JDK 7.x

Installation steps:
  # Clone the repository
  # Install the maven plugins: cd maven-plugins && mvn clean install && cd ..
  # Install LMF in your local Maven repository: mvn clean install
  # Run "mvn tomcat7:run" to start the LMF in Tomcat 7

Maybe you could find also useful these development tricks:

  http://code.google.com/p/lmf/wiki/DevelopmentTricks

Common Database settings
------------------------

(change name of database if different):

H2:
  * Hibernate: org.hibernate.dialect.H2Dialect
  * Driver: org.h2.Driver
  * URL:  jdbc:h2:/tmp/kiwi/db/kiwi;MVCC=true;DB_CLOSE_ON_EXIT=FALSE

PostgreSQL:
  * Hibernate: org.hibernate.dialect.PostgreSQLDialect
  * Drver: org.postgresql.Driver
  * URL:  jdbc:postgresql://localhost:5432/lmf

MySQL:
  * Hibernate: org.hibernate.dialect.MySQLDialect
  * Driver: com.mysql.jdbc.Driver
  * URL:  jdbc:mysql://localhost:3306/lmf

