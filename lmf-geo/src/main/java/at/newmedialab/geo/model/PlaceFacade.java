/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.geo.model;

import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.model.Namespaces;

/**
 * User: Thomas Kurz
 * Date: 13.04.11
 * Time: 11:05
 */


@RDFType(Namespaces.NS_GEONAMES+"P")
public interface PlaceFacade extends GeoPointFacade {

	@RDF(Namespaces.NS_GEONAMES+"name")
	public String getName();
	public void setName(String name);

	@RDF(Namespaces.NS_GEONAMES+"countryCode")
	public String getCountryCode();
	public void setCountryCode(String countryCode);
}
