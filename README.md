# Linked Media Framework

**HIGHLIGHT: the core development of this project has been moved to [Apache Marmotta](http://marmotta.apache.org)**

The Linked Media Framework is an easy-to-setup server application that bundles together some key open source projects to offer some advanced services for linked media management. 

## LMF Usage Scenarios 

The LMF has been designed with a number of typical use cases in mind.  We currently support the following tasks out of the box:

* [Publishing Legacy Data as Linked Data](https://bitbucket.org/srfgkmt/lmf/wiki/Guide-Publishing-Linked%20Data): whenever you have legacy datasets in CSV, Excel, XML or similar and want to publish it as Linked Data, the LMF framework is the right tool for you. Follow this guide to see how.
* [Building Semantic Search over your Data](https://bitbucket.org/srfgkmt/lmf/wiki/Guide-Semantic-Search): you have data in some format and want to enrich it with content from the Linked Data Cloud to provide advanced Semantic Search? Follow this guide!
* [Using a SKOS Thesaurus for Information Extraction](https://bitbucket.org/srfgkmt/lmf/wiki/GuideSKOSThesaurus): you have a custom thesaurus and want to analyse and automatically interlink content based on its concepts? See how to do it in this guide.

Target groups are a in particular casual users who are not experts in Semantic Web technologies but still want to publish or work with Linked Data, e.g. in the Open Government Data and Linked Enterprise Data area.

## Linked Media principles

Following the [Linked Data Principles](http://linkeddatabook.com/editions/1.0/) to expose data:

1. Use URIs as names for things.
2. Use HTTP URIs, so that people can look up those names.
3. When someone looks up a URI, provide useful information, using the standards (RDF, SPARQL).
4. Include links to other URIs, so that they can discover more things.

LMF proposes to go beyond by extending them with Linked Data Updates and by integrating management of metadata and content and making both accessible in a uniform way. The extensions are described in more detail in our [Linked Media Principles](https://bitbucket.org/srfgkmt/lmf/wiki/Principles-Linked-Media).

## LMF Architecture

LMF is build on top of three Apache projects:

* [Apache Marmotta](http://marmotta.apache.org) provides the Lined Data Platform capabilities
* [Apache Stanbol](http://stanbol.apache.org) is the extraction and enhancement framework used
* [Apache Solr](http://lucene.apache.org/solr) provides indexation capabilities

The glue that LMF implements allows to get the best of these three projects for providing advance linked media capabilities, such as semantic search or semantic enrichment. 

## LMF Modules

LMF consists of some modules, some of them optional, that can be used to extend the functionality of the Linked Media Server:

Implemented:

* [LMF Semantic Search](https://bitbucket.org/srfgkmt/lmf/wiki/Module-Semantic-Search)  offers a highly configurable *Semantic Search* service based on Apache SOLR. Several semantic search indexes can be configured in the same LMF instance. Setting up and using the Semantic Search component is described in ModuleSemanticSearch, the path language used for configuring it is described on the [LDPath](http://marmotta.apache.org/ldpath) webpage.
*[LMF Text Classification](https://bitbucket.org/srfgkmt/lmf/wiki/Module-Classifier) provides basic statistical text classification services; multiple classifiers can be created, trained with sample data and used to classify texts into categories
* [LMF Stanbol Integration](https://bitbucket.org/srfgkmt/lmf/wiki/Module-Stanbol) allows integrating with Apache Stanbol for content analysis and interlinking; the LMF provides some automatic configuration of Stanbol for common tasks
* LMF SKOS Editor allows to directly display and update SKOS thesauruses imported in the Linked Media Framework using the Open Source [SKOSjs](https://github.com/tkurz/skosjs) editor.

## LMF Client Library

A convenient option to use the LMF for Linked Data development is to build applications based on the LMF Client Library and a standalone LMF Server (can be either the generic server or a custom server as described above). The LMF Client Library provides an API abstraction around the LMF webservices and is currently available in Java, PHP, and Javascript. While each language-specific implementation retains the same general API structure, the way of accessing the API functionality is customised to the respective environment, so that e.g. a PHP developer can access the LMF in a similar way to other PHP libraries. No in-depth knowledge of RDF, Linked Data, or the Linked Media Framework is required. The library provides the following functionalities:

* Resource Management: retrieving, updating and deleting resources and associated content/metadata
* Configuration: reading and updating the LMF system configuration
* LDPath: querying the LMF and the Linked Data Cloud using path expressions or path programs
* SPARQL: querying and updating the LMF using SPARQL 1.1 Queries/Updates
* Semantic Search: configuring semantic search cores and performing queries
* Reasoner: uploading and removing rule programs

Please refer to the API definition at the code repository to see the available methods and some examples.

## Screencasts

### Screencast 1: Publishing Legacy Data as Linked Data

This [screencast](http://www.youtube.com/watch?v=_3BmNcHW4Ew) describes how to publish legacy data available as CSV, Excel, etc as Linked Data using Google Refine, DERI's RDF extension and our own Linked Media Framework.

Technologies used in this Screencast:

* [Google Refine](http://code.google.com/p/google-refine/)
* [DERI's RDF Extension](http://refine.deri.ie)
* Linked Media Framework 

### Screencast 2: Configuring Semantic Search

This [screencast](http://www.youtube.com/watch?v=s1olm2LibBI) describes how to configure the semantic search component to create a sophisticated semantic search over data in the LMF by enriching the search index with data from the Linked Data Cloud.

Technologies used in this Screencast:

* Linked Media Framework
* [LDPath](http://marmotta.apache.org/ldpath)
* [AJAX-SOLR](https://github.com/evolvingweb/ajax-solr)

## Real-World Installations

Even though just published, the Linked Media Framework is already used in some real-world and some demo installations:

* Salzburger Nachrichten Semantic Search (http://search.salzburg.com ): more than 1 million news articles published as Linked Data and connected with the Linked Data Cloud, available through a facetted search interface
* Salzburg NewMediaLab Labs Server (http://labs.newmedialab.at ): some of our own demo szenarios for experimenting with the LMF capabilities


## Testimonials

François-Paul Servant, Renault: 

 "You cannot imagine how happy I've been to see such a great and neat demo. You know, I've been a long time believer in SW, but I was beginning to give up hope on getting well polished tools - useful things that work and look good. This demo changed my mind. Thanks! I can't wait to have LMF installed on my machine and use it!"


----

This software has been brought to you by the Knowledge and Media Technologies group at [Salzburg Research](http://www.salzburgresearch.at) and was funded within the [Salzburg NewMediaLab](http://www.newmedialab.at) K-project.

[YourKit](http://www.yourkit.com) is kindly supporting open source projects with its full-featured Java Profiler. !YourKit, LLC is the creator of innovative and intelligent tools for profiling Java and .NET applications. Take a look at !YourKit's leading software products: [YourKit Java Profiler](http://www.yourkit.com/java/profiler/index.jsp) and [YourKit .NET Profiler](http://www.yourkit.com/.net/profiler/index.jsp).

[ZeroTurnaround](http://zeroturnaround.com) is kindly supporting an [open source license](http://zeroturnaround.com/software/jrebel/resources/faq/#36) for using [JRebel](http://zeroturnaround.com/software/jrebel/) in the development of LMF.