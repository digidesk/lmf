/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
	/**
	 * AjaxSolr.MapWidget displays the geo-pos of the search result on a google-maps map.
	 * Requires http://maps.googleapis.com/maps/api/js and jQuery
	 */
	AjaxSolr.MapWidget = AjaxSolr.AbstractWidget.extend({
		// default start pos is SRFG ;-)
		startPos: [47.635784, 13.590088],
		resultSelector: 'div.result',
		resultIdPrefix: 'doc-',
		resultHighlightClass: 'hoover',
		field: ['lat', 'lng'],
		map: null,
		mapOptions: null,
		region: null,
		regionOptions: null,
		init: function() {
			var self = this
			$(this.resultSelector).live('mouseover', function() {
				var id = $(this).attr('id');
				self.markerBounce(true, id);
			});
			$(this.resultSelector).live('mouseout', function() {
				var id = $(this).attr('id');
				self.markerBounce(false, id);				
			});
			
			var container = $(this.target);
			container.empty();
			var opts = this.mapOptions || {
					zoom: 6,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					disableDefaultUI: true
			};
			var iniLoc = new google.maps.LatLng(this.startPos[0], this.startPos[1]);
			this.map = new google.maps.Map(container[0], opts);
			this.map.setCenter(iniLoc);
			this.marks = [];
			
			var isCircling = false;
			var adjustRadius = function(event) {
				if (self.region) {
					var center = self.region.getCenter();
					var dist = google.maps.geometry.spherical.computeDistanceBetween(center, event.latLng);
					if (dist < 10) {
						self.region.setMap(null);
						self.region = null;
					} else {
						self.region.setRadius(dist);
					}
				}
			};
			var createCircle = function(event) {
				if (isCircling) {
					adjustRadius(event);
					self.manager.doRequest(0);
				} else {
					if (self.region) {
						self.region.setMap(null);
					}
					var cOpts = self.regionOptions || {
							strokeColor: "#FF0000",
							strokeOpacity: 0.8,
							strokeWeight: 2,
							fillColor: "#FF0000",
							fillOpacity: 0.35
					};
					cOpts.map = self.map;
					cOpts.center = event.latLng;
					
					self.region = new google.maps.Circle(cOpts);
					google.maps.event.addListener(self.region, 'mousemove', function(event) {
						if (isCircling) {
							adjustRadius(event);
						}
					});
					google.maps.event.addListener(self.region, 'rightclick', createCircle);
				}
				isCircling = !isCircling;
			};
			google.maps.event.addListener(this.map, 'rightclick', createCircle);
			google.maps.event.addListener(this.map, 'mousemove', function(event) {
				if (isCircling) {
					adjustRadius(event);
				}
			});
			google.maps.event.addListener(this.map, 'mouseout', function(event) {
				if (isCircling) {
					self.region.setMap(null);
					self.region = null;
					isCircling = false;
				}
			});

		},
		beforeRequest: function() {
			this.manager.store.removeByValue('fq', new RegExp('^\\{\\!geofilt sfield=' + this.field + ' pt='));
			if (typeof this.field == 'string' && this.region) {
				var center = this.region.center.lat() + ',' + this.region.center.lng();
				var dist = this.region.radius / 1000 ; // google uses [m], solr [km]
				this.manager.store.addByValue('fq', '{!geofilt sfield=' + this.field + ' pt=' +center+ ' d=' + dist + '}');
			}
		},
		_hasLocation: function(doc) {
			if ($.isArray(this.field)) {
				return (doc[this.field[0]] !== undefined && doc[this.field[1]] !== undefined);
			} else if (typeof this.field == 'string') {
				return (doc[this.field] !== undefined);
			} 
			return false;
		},
		_getLatLon: function(doc) {
			if ($.isArray(this.field)) {
				return new google.maps.LatLng(doc[this.field[0]][0], doc[this.field[1]][0]);
			} else if (typeof this.field == 'string') {
				var lalo = (($.isArray(doc[this.field]))?doc[this.field][0]:doc[this.field]).split(',');
				return new google.maps.LatLng(lalo[0], lalo[1]);
			}
			return null;
		},
		afterRequest: function () {
			// Remove old markers
			for(i in this.marks) {
				this.marks[i].setMap(null);
			}
			this.marks = [];
			// Add markers
			var region = new google.maps.LatLngBounds();
			var docs = this.manager.response.response.docs;
			var self = this;

			for(i in docs) {
				var doc = docs[i];
				if (this._hasLocation(doc)) {
					var pos = this._getLatLon(doc);
					var marker = this.createMarker(doc);
					marker.res_id = doc.id;
					google.maps.event.addListener(marker, 'mouseover', function() {
						self.resultHoover(this.res_id);
					});
					google.maps.event.addListener(marker, 'mouseout', function() {
						self.resultHoover(null);
					});
					google.maps.event.addListener(marker, 'click', function() {
						var entry = $("#" + self.resultIdPrefix + this.res_id);
						if (entry.length > 0) $("html").scrollTop(entry.offset().top);
					});
					marker.setPosition(pos);
					region.extend(pos);
					marker.setMap(this.map);
					this.marks[this.resultIdPrefix + doc.id] = marker;
				}
			}
			if (self.region) {
				self.map.fitBounds(self.region.getBounds());
			}
			if (!region.isEmpty()) {
				self.map.panTo(region.getCenter());
				var mapBounds = self.map.getBounds();
				if (mapBounds && !(mapBounds.contains(region.getNorthEast()) && mapBounds.contains(region.getSouthWest()))) {
					self.map.fitBounds(region);
				}
			}
		},
		resultHoover: function(id) {
			$(this.resultSelector).removeClass(this.resultHighlightClass);
			if (id === undefined || id == null) {
			} else {
				$("#" + this.resultIdPrefix + id).addClass(this.resultHighlightClass);
			}
		},
		markerBounce: function(on, id) {
			if (on === true) {
				if (id && this.marks[id]) {
					this.marks[id].setAnimation(google.maps.Animation.BOUNCE);
				}
			} else if (on === false) {
				if (id && this.marks[id]) {
					this.marks[id].setAnimation(null);
				} else {
					for(i in this.marks) {
						this.marks[i].setAnimation(null);
					}
				}
			}
		},
		createMarker: function(doc) {
			var marker = new google.maps.Marker({
				title: doc.title + ""
			});
			return marker;
		},
		multivalue: false
	});
})(jQuery);