/*
 * Copyright (c) 2012 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function( $ ){
    $.fn.lmf_login = function(options) {
        var settings = {
            host: 'http://localhost:8080/LMF/',
            lmflogin : 'auth/login',
            mime_type: 'application/xhtml+xml',
            redirectTo: 'http://localhost:8080/LMF/social/',
            fblogin : 'auth/oauth2/facebook',
            ytlogin : 'auth/oauth2/youtube'
        }

        /**
         * main method
         */
        return this.each(function() {
            // merge options
            if ( options ) {
                $.extend( settings, options );
            }
            var title = $("<div id='widgetTitle'>Login</div><hr/>");
            
            var div = $("<div id='lmfLoginForm'></div>");

            if(settings.redirectTo != '') {
                var lmfLoginLink = settings.host + settings.lmflogin + "?redirect_uri=" + encodeURIComponent(settings.redirectTo);
            } else {
                var lmfLoginLink = settings.host + settings.lmflogin;
            }
            var lmfUsername = $("<span style='text-align: left;'>Username: <input type=\"text\" name=\"username\"/></span>");

            var lmfPassword = $("<span style='text-align: left;'>Password: <input type=\"password\" name=\"password\"/></span>");

            var hiddenFieldClientId = $("<input style='margin-top: 5px; width: 70px;' type=\"hidden\" value=\"1\" name=\"client_id\"/>");
            var hiddenFieldGrantType = $("<input style='margin-top: 5px; width: 70px;' type=\"hidden\" value=\"password\" name=\"grant_type\"/>");

            var lmfLoginButton = $("<input style='margin-top: 5px; width: 70px;' type=\"submit\" value=\"Login\"/></span>");
            var paragraph_lmf = $("<form id=\"lmf-login\" method=\"POST\" action=\""+lmfLoginLink+"\"></form>").append(lmfUsername);

            paragraph_lmf.append(lmfPassword);
            paragraph_lmf.append(hiddenFieldGrantType);
            paragraph_lmf.append(hiddenFieldClientId);
            paragraph_lmf.append(lmfLoginButton);


            var paragraph_alt = $("<p id=\"alt_login\">").append("Or log in via: <br/>");

            var fbURL = settings.host + settings.fblogin;
            var ytURL = settings.host + settings.ytlogin;

            var firstParam = true;

            if(settings.redirectTo != '') {
                firstParam = false;
                fbURL += '?redirect_uri=' + encodeURIComponent(settings.redirectTo);
                ytURL += '?redirect_uri=' + encodeURIComponent(settings.redirectTo);
            }

            if(settings.mime_type != '') {
                if(firstParam) {
                    fbURL += '?';
                    ytURL += '?';
                } else {
                    fbURL += '&';
                    ytURL += '&';
                }
                fbURL += 'mime_type=' + settings.mime_type;
                ytURL += 'mime_type=' + settings.mime_type;
            }

            var fbLoginLink = $("<a href=\""+ fbURL +"\"> <img src=\"img/icons/facebook-icon.png\" alt=\"Facebook\" width=\"30px\" height=\"30px\"/></a>");
//            var fbLoginLink = $("<a onclick='"+request.login(fbURL)+"'> <img src=\"img/icons/facebook-icon.png\" alt=\"Facebook\" width=\"30px\" height=\"30px\"/></a>");

            var ytLoginLink = $("<a href=\""+ ytURL +"\"><img src=\"img/icons/youtube-icon.png\" alt=\"Youtube\" width=\"30px\" height=\"30px\"/></a>");

            paragraph_alt.append(fbLoginLink);
            paragraph_alt.append(ytLoginLink);

            //append elements
            $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'widgets/socialnetwork/hacksAndDemos/lmfLoginWidget.css') );

            $(this).html('');
            div.append(title);
            div.append(paragraph_lmf);
            div.append(paragraph_alt);

            $(this).append(div);
        });
    };
})( jQuery );

