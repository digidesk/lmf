/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.services.importer;

import org.apache.marmotta.platform.core.api.importer.Importer;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.slf4j.Logger;

import at.newmedialab.lmf.social.api.rss.RSSService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Sebastian Schaffert
 * @author Jakob Frank <jakob.frank@salzburgresearch.at>
 *     //TODO add context to rss service methods
 * 
 */
public class RSSImporterImpl implements Importer {

    @Inject
    private Logger                   log;

    @Inject
    private RSSService               rssService;

    private static final Set<String> MIME_TYPES;
    static {
        HashSet<String> mt = new HashSet<String>();
        mt.add("application/vnd.lmf.rss+xml");
        mt.add("application/vnd.lmf.atom+xml");
        mt.add("application/vnd.lmf.x-georss+xml");
        mt.add("application/vnd.lmf.x-geoatom+xml");

        MIME_TYPES = Collections.unmodifiableSet(mt);
    };

    @PostConstruct
    public void initialise() {
        log.info("registering RSS/ATOM importer ...");
    }

    /**
     * Get a collection of all mime types accepted by this importer. Used for automatically
     * selecting the appropriate importer in ImportService.
     * 
     * @return a set of strings representing the mime types accepted by this importer
     */
    @Override
    public Set<String> getAcceptTypes() {
        return MIME_TYPES;
    }

    /**
     * Get a description of this importer for presentation to the user.
     * 
     * @return a string describing this importer for the user
     */
    @Override
    public String getDescription() {
        return "Importer RSS and Atom feeds";
    }

    /**
     * Get the name of this importer. Used for presentation to the user and for internal
     * identification.
     * 
     * @return a string uniquely identifying this importer
     */
    @Override
    public String getName() {
        return "RSS/ATOM";
    }

    /**
     * Import data from the input stream provided as argument into the KiWi database.
     *
     * @param url the url from which to read the data
     * @param user the user to use as author of all imported data
     */
    @Override
    public int importData(URL url, String format, Resource user, URI context) {

        // We try geocoding only on explicit request
        if (format.contains("/x-geo")) {
            try {
                log.info("geocoding feed");
                final String geoUrl = "http://ws.geonames.org/rssToGeoRSS?feedUrl="
                        + URLEncoder.encode(url.toExternalForm(), "utf-8");

                return rssService.importFeed(new URL(geoUrl), (URI)user);
            } catch (Exception ex) {
                log.info("geocoding RSS feed failed (message: {}); trying normal RSS feed", ex.getMessage());
                return rssService.importFeed(url, (URI)user);
            }
        } else
            return rssService.importFeed(url, (URI)user);

    }

    /**
     * Import data from the input stream provided as argument into the KiWi database.
     *
     * @param is the input stream from which to read the data
     * @param user the user to use as author of all imported data
     */
    @Override
    public int importData(InputStream is, String format, Resource user, URI context) {
        return rssService.importFeed(new InputStreamReader(is), (URI)user);
    }

    /**
     * Import data from the reader provided as argument into the KiWi database.
     *
     * @param reader the reader from which to read the data
     * @param user the user to use as author of all imported data
     */
    @Override
    public int importData(Reader reader, String format, final Resource user, URI context) {
        return rssService.importFeed(reader, (URI)user);
    }
}
