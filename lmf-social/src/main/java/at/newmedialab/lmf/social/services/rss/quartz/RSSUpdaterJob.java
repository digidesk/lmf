/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.services.rss.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;

import at.newmedialab.lmf.scheduler.api.DependencyInjectedJob;
import at.newmedialab.lmf.social.api.rss.RSSService;

import javax.inject.Inject;

public class RSSUpdaterJob implements DependencyInjectedJob {

    @Inject
    private Logger             log;

    @Inject
    private RSSService         rssService;

    private String             feedUrl = null;

    public void setFeedUrl(String feedUrl) {
        this.feedUrl = feedUrl;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        if (feedUrl == null) return;

        int c = rssService.updateFeed(feedUrl);
        log.info("updated feed <{}> ({} new items)", feedUrl, c);
    }
}