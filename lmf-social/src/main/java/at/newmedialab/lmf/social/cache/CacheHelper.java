/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.cache;

import org.apache.marmotta.platform.ldpath.api.LDPathService;
import org.openrdf.model.Value;

/**
 * Helper to allow social media module easily work with LDCache
 * 
 * @author Sergio Fernández
 *
 */
public class CacheHelper {
	
	/**
	 * Force the cache to update the context according the given ldpath program
	 * 
	 * @param ldpath
	 * @param context
	 * @param program
	 */
	public static void update(LDPathService ldpath, Value context, String program) {
		CacheUpdate update = new CacheUpdate(ldpath, context, program);
        Thread thread = new Thread(update, "CacheHelper.update");
        thread.start();
	}
	
	private static class CacheUpdate implements Runnable {

	    private LDPathService ldpath;
	    private Value context;
	    private String program;

	    public CacheUpdate(LDPathService ldpath, Value context, String program) {
	        this.ldpath = ldpath;
	        this.context = context;
	        this.program = program;
	    }

	    public void run() {
	    	try {
				ldpath.programQuery(context, program);
			} catch (Exception e) {
				
			}
	    }
	}

}
