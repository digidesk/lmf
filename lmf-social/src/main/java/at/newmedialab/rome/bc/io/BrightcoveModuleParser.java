/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.rome.bc.io;

import at.newmedialab.rome.bc.BrightcoveModule;
import at.newmedialab.rome.bc.BrightcoveModuleImpl;
import com.sun.syndication.feed.module.Module;
import com.sun.syndication.io.ModuleParser;
import org.jdom2.Element;
import org.jdom2.Namespace;

public class BrightcoveModuleParser implements ModuleParser {

    private static final Namespace NS  = Namespace.getNamespace(BrightcoveModule.URI);

    @Override
    public String getNamespaceUri() {
        return BrightcoveModule.URI;
    }

    @Override
    public Module parse(Element root) {
        // bc only valid on item-level.
        if ("item".equals(root.getName())) {
            BrightcoveModuleImpl mod = new BrightcoveModuleImpl();

            mod.setPlayerId(root.getChildTextNormalize("playerid", NS));
            mod.setLineupId(root.getChildTextNormalize("lineupid", NS));
            mod.setTitleId(root.getChildTextNormalize("titleid", NS));
            mod.setAccountId(root.getChildTextNormalize("accountid", NS));

            String d = root.getChildTextNormalize("duration", NS);
            if (d != null) {
                try {
                    mod.setDuration(new Long(d));
                } catch (NumberFormatException e) {
                }
            }

            return mod;
        }
        return null;
    }

}
