Parts of the NLP module have dependencies to external data models that are not completely free to use. In
certain cases a separate license needs to be acquired:

Sentiment Analysis:

The Sentiment Analysis services make use of external models that are only free for non-commercial use. If
you use the LMF Sentiment Analysis in commercial settings you need to acquire licenses for:
- German sentiment analysis makes use of SentiWS; SentiWS is licensed under a Creative Commons Noncommercial
  Share Alike license; commercial licenses can be acquired at http://asv.informatik.uni-leipzig.de/download/sentiws.html
- English sentiment analysis makes use of SentiWordNet, which is available for educational and research use. A
  commercial license can be acquired at http://sentiwordnet.isti.cnr.it/


