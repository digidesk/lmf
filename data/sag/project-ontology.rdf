<!--
  ~ Copyright (c) 2012 Salzburg Research.
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<!DOCTYPE rdf:RDF [
<!ENTITY lmf "http://localhost:8080/LMF/resource/">]>

<rdf:RDF
        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
        xmlns:owl="http://www.w3.org/2002/07/owl#"
        >

    <owl:Class about="&lmf;concepts/Project">
        <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Project"/>
        <rdfs:label>Project</rdfs:label>
        <rdfs:comment>A project</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/EUProject">
        <rdfs:subClassOf resource="&lmf;concepts/Project"/>
        <rdfs:label>EU Project</rdfs:label>
        <rdfs:comment>A European Union funded project</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/NationalProject">
        <rdfs:subClassOf resource="&lmf;concepts/Project"/>
        <rdfs:label>National Project</rdfs:label>
        <rdfs:comment>A nationally funded project</rdfs:comment>
    </owl:Class>


    <owl:Class about="&lmf;concepts/Deliverable">
        <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Document"/>
        <rdfs:label>Deliverable</rdfs:label>
        <rdfs:comment>A deliverable belonging to a project</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/Contract">
        <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Document"/>
        <rdfs:label>Contract</rdfs:label>
        <rdfs:comment>A (part of a) contract belonging to a project</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/Proposal">
        <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Document"/>
        <rdfs:label>Proposal</rdfs:label>
        <rdfs:comment>A (part of a) proposal belonging to a project</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/Budget">
        <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Document"/>
        <rdfs:label>Budget</rdfs:label>
        <rdfs:comment>A document belonging to the budget calculation of a project</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/Report">
        <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Document"/>
        <rdfs:label>Report</rdfs:label>
        <rdfs:comment>A project report</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/Dissemination">
        <rdfs:label>Dissemination</rdfs:label>
        <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Document"/>
        <rdfs:comment>A document used for public relations or dissemination in a project</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/Meeting">
        <rdfs:label>Meeting</rdfs:label>
        <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Document"/>
        <rdfs:comment>Meeting minutes or meeting resources of a project meeting</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/Archive">
        <rdfs:label>Archive</rdfs:label>
        <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Document"/>
        <rdfs:comment>Archived document</rdfs:comment>
    </owl:Class>

    <owl:Class about="&lmf;concepts/Final">
         <rdfs:label>Final</rdfs:label>
         <rdfs:subClassOf resource="http://xmlns.com/foaf/0.1/Document"/>
         <rdfs:comment>Final document</rdfs:comment>
     </owl:Class>


    <rdf:Property rdf:about="&lmf;concepts/projectFolder">
        <rdfs:label>Project Folder</rdfs:label>
        <rdfs:comment>The name of the project folder in the file system hierarchy</rdfs:comment>
        <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#DatatypeProperty"/>
        <rdfs:domain rdf:resource="&lmf;concepts/Project"/>
        <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#string"/>
    </rdf:Property>

    <rdf:Property rdf:about="&lmf;concepts/project">
        <rdfs:label>Project</rdfs:label>
        <rdfs:comment>The project a document belongs to</rdfs:comment>
        <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#DatatypeProperty"/>
        <rdfs:range rdf:resource="http://xmlns.com/foaf/0.1/Project"/>
        <rdfs:domain rdf:resource="http://xmlns.com/foaf/0.1/Document"/>
    </rdf:Property>

    <rdf:Property rdf:about="&lmf;concepts/kind">
        <rdfs:label>Kind of Document</rdfs:label>
        <rdfs:comment>The kind of document</rdfs:comment>
        <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#DatatypeProperty"/>
        <rdfs:range rdf:resource="http://xmlns.com/foaf/0.1/Document"/>
        <rdfs:domain rdf:resource="http://xmlns.com/foaf/0.1/Document"/>
    </rdf:Property>


</rdf:RDF>
