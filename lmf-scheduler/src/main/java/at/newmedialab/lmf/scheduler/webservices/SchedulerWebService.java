/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.scheduler.webservices;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.marmotta.commons.sesame.model.Namespaces;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;

import at.newmedialab.lmf.scheduler.api.QuartzService;
import at.newmedialab.lmf.scheduler.webservices.json.JsonJob;
import at.newmedialab.lmf.scheduler.webservices.json.JsonTrigger;

@Path("/quartz")
public class SchedulerWebService {

    @Inject
    Logger        log;

    @Inject
    QuartzService quartzService;

    @GET
    @Path("/triggers")
    @Produces(Namespaces.MIME_TYPE_JSON)
    public HashMap<String, HashSet<JsonTrigger>> getTriggers() {
        try {
            final HashMap<String, HashSet<JsonTrigger>> triggers = new HashMap<String, HashSet<JsonTrigger>>();
            final List<String> groups = quartzService.getTriggerGroupNames();
            for (String group : groups) {
                HashSet<JsonTrigger> t = new HashSet<JsonTrigger>();
                final Set<TriggerKey> triggerKeys = quartzService.getTriggerKeys(GroupMatcher.triggerGroupEquals(group));
                for (TriggerKey triggerKey : triggerKeys) {
                    final Trigger trigger = quartzService.getTrigger(triggerKey);
                    t.add(new JsonTrigger(trigger, quartzService.getJobDetail(trigger.getJobKey())));
                }
                triggers.put(group, t);
            }
            return triggers;
        } catch (SchedulerException e) {
            log.warn("Could not access Scheduler: {}", e);
        }
        return new HashMap<String, HashSet<JsonTrigger>>();
    }

    @GET
    @Path("/jobs")
    @Produces(Namespaces.MIME_TYPE_JSON)
    public HashMap<String, HashSet<JsonJob>> getJobs() {
        final HashMap<String, HashSet<JsonJob>> jobs = new HashMap<String, HashSet<JsonJob>>();

        try {
            final List<String> groups = quartzService.getJobGroupNames();
            for (String group : groups) {
                HashSet<JsonJob> j = new HashSet<JsonJob>();
                final Set<JobKey> jobKeys = quartzService.getJobKeys(GroupMatcher.jobGroupEquals(group));
                for (JobKey jobKey : jobKeys) {
                    j.add(new JsonJob(quartzService.getJobDetail(jobKey), quartzService.getTriggersOfJob(jobKey)));
                }
                jobs.put(group, j);
            }
        } catch (SchedulerException e) {
            log.warn("Could not access Scheduler: {}", e);

        }

        return jobs;
    }
}
