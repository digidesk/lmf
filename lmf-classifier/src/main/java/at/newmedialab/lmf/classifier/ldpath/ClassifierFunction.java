/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.classifier.ldpath;

import at.newmedialab.ldpath.api.backend.RDFBackend;
import at.newmedialab.ldpath.model.transformers.DoubleTransformer;
import at.newmedialab.ldpath.model.transformers.StringTransformer;
import at.newmedialab.lmf.classifier.api.ClassificationService;
import at.newmedialab.lmf.classifier.exception.ClassificationException;
import at.newmedialab.lmf.classifier.model.Classification;
import at.newmedialab.lmf.ldpath.api.LMFLDPathFunction;
import com.google.common.base.Preconditions;
import kiwi.core.model.rdf.KiWiNode;
import kiwi.core.util.KiWiCollections;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * An LDPath Function that allows to classify the text passed as argument. The result will be the URI of the
 * category that matches best to the text. Optionally, a threshold can be given, in which case the result will be
 * a list of URIs of categories that match with at least the given threshold. General function signature:
 * <ul>
 *     <li>classify("classifier name", LDPath Expression) - to return the best matching category for the text selected by the expression</li>
 *     <li>classify("classifier name", LDPath Expression, Threshold) - to return all categories matching with higher certainty than the given threshold</li>
 * </ul>
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class ClassifierFunction implements LMFLDPathFunction {

    @Inject
    private Logger log;

    @Inject
    private ClassificationService classificationService;

    private final StringTransformer<KiWiNode> strTransformer = new StringTransformer<KiWiNode>();

    private final DoubleTransformer<KiWiNode> dblTransformer = new DoubleTransformer<KiWiNode>();

    public ClassifierFunction() {
    }

    @PostConstruct
    public void initialise() {
        log.info("initialising LMF LDPath fn:classify(...) function ...");
    }


    /**
     * Apply the function to the list of nodes passed as arguments and return the result as type T.
     * Throws IllegalArgumentException if the function cannot be applied to the nodes passed as argument
     * or the number of arguments is not correct.
     *
     * @param context the context of the execution. Same as using the
     *                {@link at.newmedialab.ldpath.api.selectors.NodeSelector} '.' as parameter.
     * @param args    a nested list of KiWiNodes
     * @return
     */
    @Override
    public Collection<KiWiNode> apply(RDFBackend<KiWiNode> kiWiNodeRDFBackend, KiWiNode context, Collection<KiWiNode>... args) throws IllegalArgumentException {
        Preconditions.checkArgument(args.length >= 2);
        Preconditions.checkArgument(args.length <=3);

        StringBuilder text = new StringBuilder();
        for(KiWiNode n : args[1]) {
            text.append(strTransformer.transform(kiWiNodeRDFBackend, n));
            text.append(" ");
        }
        
        String classifier = strTransformer.transform(kiWiNodeRDFBackend, KiWiCollections.first(args[0]));
        

        try {
            List<KiWiNode> result = new LinkedList<KiWiNode>();
            if(args.length == 2) {
               result.add(classificationService.getBestClassification(classifier,text.toString().replaceAll("\\n"," ")));
            } else if(args.length == 3) {

                double threshold = dblTransformer.transform(kiWiNodeRDFBackend, KiWiCollections.first(args[2]));
                
                for(Classification c : classificationService.getAllClassifications(classifier,text.toString().replaceAll("\\n"," "),threshold)) {
                    result.add(c.getCategory());
                }
            }
            return result;
        } catch (ClassificationException e) {
            log.error("error while classifying text",e);
            return Collections.emptyList();
        }

    }

    /**
     * Return the representation of the NodeFunction or NodeSelector in the RDF Path Language
     *
     * @param kiWiNodeRDFBackend
     * @return
     */
    @Override
    public String getPathExpression(RDFBackend<KiWiNode> kiWiNodeRDFBackend) {
        return "classify";
    }

    /**
     * A string describing the signature of this node function, e.g. "fn:content(uris : Nodes) : Nodes". The
     * syntax for representing the signature can be chosen by the implementer. This method is for informational
     * purposes only.
     *
     * @return
     */
    @Override
    public String getSignature() {
        return "fn:classify(classifier_name : String, nodes : LiteralList [, threshold : Double]) : NodeList";
    }

    /**
     * A short human-readable description of what the node function does.
     *
     * @return
     */
    @Override
    public String getDescription() {
        return "Classify the content of the literal nodes passed as second argument using the classifier passed as first argument.";
    }
}
