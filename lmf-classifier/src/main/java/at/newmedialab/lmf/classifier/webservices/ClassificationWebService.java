/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.classifier.webservices;

import at.newmedialab.lmf.classifier.api.ClassificationService;
import at.newmedialab.lmf.classifier.exception.ClassificationException;
import at.newmedialab.lmf.classifier.exception.ClassifierNotFoundException;
import at.newmedialab.lmf.classifier.model.Classification;
import at.newmedialab.lmf.classifier.model.Classifier;
import com.google.common.io.CharStreams;
import kiwi.core.api.triplestore.ResourceService;
import kiwi.core.model.rdf.KiWiUriResource;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;

/**
 * A webservice supporting classification using different classifiers.
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
@Path("/classifier")
public class ClassificationWebService {

    @Inject
    private Logger log;


    @Inject
    private ClassificationService classificationService;

    @Inject
    private ResourceService resourceService;


    /**
     * Return a JSON list of all classifier names currently registered in the LMF Classifier module.
     * 
     * @HTTP 200 in case the classifiers can be returned
     * @HTTP 500 in case an error occurs
     * 
     * @return a JSON list of classifier string labels
     */
    @GET
    @Path("/list")
    @Produces("application/json")
    public Response listClassifiers() {
        List<String> classifiers = new ArrayList<String>();
        for(Classifier classifier : classificationService.listClassifiers()) {
            classifiers.add(classifier.getName());
        }
        return Response.ok(classifiers).build();
    }
    
    
    /**
     * Create a new classifier with the given name. The service will take care of creating the appropriate
     * configuration entries and work files in the LMF work directory.
     *
     * @param name a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     *
     * @HTTP 201 in case the classifier has been created successfully
     * @HTTP 403 in case the classifier already exists
     * @HTTP 500 in case an error occurred while creating the classifier
     *
     */
    @POST
    @Path("/{name:[a-zA-Z0-9]+}")
    public Response createClassifier(@PathParam("name") String name) {
        try {
            classificationService.createClassifier(name);

            return Response.ok("classifier "+name+" created").build();
        } catch (ClassificationException e) {
            return Response.status(Response.Status.FORBIDDEN).entity("classifier "+name+" already exists").build();
        }
    }


    /**
     * Return the description of the classifier identified by the name given as path argument. The
     * result will be a JSON map of the following form:
     * <code>
     *     { name : CLASSIFIERNAME, concepts: [concept_uri1, concept_uri2, ...]}
     * </code>
     * 
     * @HTTP 200 in case the classifier description could be returned
     * @HTTP 404 in case the classifier does not exist
     * @HTTP 500 in case an error occurred
     * 
     * @param name the name of the classifier
     * @return  a JSON map describing the classifier as illustrated above
     */
    @GET
    @Path("/{name:[a-zA-Z0-9]+}")
    @Produces("application/json")
    public Response getClassifier(@PathParam("name") String name) {
        try {
            Classifier classifier = classificationService.getClassifier(name);
            
            Map<String,Object> result = new HashMap<String, Object>();
            result.put("name",classifier.getName());
            
            List<String> concepts = new ArrayList<String>();
            for(KiWiUriResource resource : classifier.getCategories()) {
                concepts.add(resource.getUri());
            }
            result.put("concepts",concepts);

            return Response.ok(result).build();
            
        } catch (ClassifierNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).entity("classifier with name "+name+" does not exist").build();
        } catch (ClassificationException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("server error while listing classifier "+name+": "+e.getMessage()).build();
        }
    }
    


    /**
     * Remove the classifier with the given name from the system configuration.
     *
     * @param name a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param removeData also remove all training and model data of this classifier from the file system
     *                   
     * @HTTP 200 in case the classifier has been removed successfully
     * @HTTP 404 in case the classifier does not exist
     */
    @DELETE
    @Path("/{name:[a-zA-Z0-9]+}")
    public Response removeClassifier(@PathParam("name") String name, @QueryParam("removeData") boolean removeData) {
        try {
            classificationService.removeClassifier(name,removeData);
            
            return Response.ok("classifier "+name+" removed successfully"+ (removeData?" with all data":"")).build();
        } catch (ClassificationException e) {
            return Response.status(Response.Status.NOT_FOUND).entity("classifier "+name+" not found").build();
        }
    }


    /**
     * Add training data to the classifier identified by the given name and for the concept passed as argument. Note
     * that training data is not immediately taken into account by the classifier. Retraining of the classifier will
     * take place when a certain threshold of training datasets has been added or when a certain (configurable) time has
     * passed.
     * <p/>
     * The sample text for the concept is posted as plain text to the service.
     *
     * @param name a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param conceptUri the URI of the concept which to train with the sample text
     *
     * @HTTP 200 in case the training data has been accepted
     * @HTTP 400 in case the concept or training data is not given
     * @HTTP 404 in case the classifier or the concept do not exist
     * @HTTP 500 in case an error occurred while adding the training data (error message is part of response)
     */
    @POST
    @Path("/{name:[a-zA-Z0-9]+}/train")
    @Consumes("text/plain")
    public Response addTrainingData(@PathParam("name") String name, @QueryParam("concept") String conceptUri, @Context HttpServletRequest request) {
        if(conceptUri == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("a concept URI is required").build();
        }
        if(resourceService.getUriResource(conceptUri) == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("the concept with URI "+conceptUri+" does not exist").build();
        }
        try {
            String sample_data = CharStreams.toString(request.getReader());
            if(sample_data != null && !"".equals(sample_data.trim())) {
                try {
                    classificationService.trainClassifier(name,resourceService.getUriResource(conceptUri),sample_data);
                    
                    return Response.ok("training data added for classifier "+name+" and concept "+conceptUri).build();
                } catch (ClassifierNotFoundException e) {
                    return Response.status(Response.Status.NOT_FOUND).entity("the classifier "+name+" does not exist").build();
                } catch (ClassificationException e) {
                    return Response.status(Response.Status.BAD_REQUEST).entity("a server error occurred while adding training data").build();
                }


            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("no sample data has been given").build();
            }


        } catch (IOException e) {
            return Response.serverError().entity("error while reading request body").build();
        }

    }

    /**
     * Immediately retrain the core with the name passed as argument, ignoring the automatic retraining timer. Used
     * when you know that there will be no more data coming and you want the classifier to be available immediately.
     * 
     * @param name the name of the classifier to retrain
     * 
     * @HTTP 200 when retraining was successful
     * @HTTP 404 when the classifier with the given name does not exist
     * @HTTP 500 when an error occurred while retraining
     */
    @POST
    @Path("/{name:[a-zA-Z0-9]+}/retrain")
    public Response retrainCore(@PathParam("name") String name) {
        try {
            classificationService.retrainClassifier(name);
            return Response.ok("retrained classifier "+name+" successfully").build();
        } catch (ClassifierNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).entity("classifier with name "+name+" does not exist").build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }



    /**
     * Get classifications from the given classifier for the given text. The classifications will be ordered by
     * descending probability, so that classifications with higher probability will be first. A classification object
     * consists of a KiWiUriResource identifying the classified concept and a probability indicating how likely it is
     * that the text matches the given concept.
     * <p/>
     * The text to classify is posted as plain text to the service.
     *
     * @param name a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param threshold the minimum probability of a classification to be considered in the result (optional, otherwise all classifications will be returned)
     * @return a JSON list of classifications ordered by descending probability; each classification is a map with the
     *         properties "concept" containing the concept URI and "probability" containing the likelihood of the
     *         classification
     *
     * @HTTP 200 in case classification succeeds
     * @HTTP 404 in case the classifier with this name does not exist
     * @HTTP 400 in case no text to classify was posted
     * @HTTP 500 in case an error occurred during classification
     */
    @POST
    @Path("/{name:[a-zA-Z0-9]+}/classify")
    @Produces("application/json")
    @Consumes("text/plain")
    public Response getClassifications(@PathParam("name") String name, @QueryParam("threshold") Double threshold, @Context HttpServletRequest request) {
        if(threshold == null) {
            threshold = 0.0;
        }
        try {
            String content = CharStreams.toString(request.getReader());
            if(content != null && !"".equals(content.trim())) {
                try {
                    List<Classification> classifications = classificationService.getAllClassifications(name,content,threshold);

                    List<Map<String,String>> result = new LinkedList<Map<String, String>>();
                    for(Classification classification : classifications) {
                        Map<String,String> c = new HashMap<String, String>();
                        c.put("concept",classification.getCategory().getUri());
                        c.put("probability",Double.toString(classification.getProbability()));

                        result.add(c);
                    }

                    return Response.ok(result).build();
                } catch (ClassifierNotFoundException e) {
                    return Response.status(Response.Status.NOT_FOUND).entity("the classifier "+name+" does not exist").build();
                } catch (ClassificationException e) {
                    return Response.status(Response.Status.BAD_REQUEST).entity("a server error occurred while classifying text").build();
                }


            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("no sample data has been given").build();
            }


        } catch (IOException e) {
            return Response.serverError().entity("error while reading request body").build();
        }
    }

}
