/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.services.enhancer;

import at.newmedialab.lmf.enhancer.api.config.EnhancerConfigService;
import at.newmedialab.lmf.enhancer.api.enhancer.EnhancerEngineService;
import at.newmedialab.lmf.enhancer.api.program.EnhancerProgramService;
import at.newmedialab.lmf.enhancer.model.enhancer.EnhancerConfiguration;
import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementResult;
import at.newmedialab.lmf.worker.services.WorkerServiceImpl;
import org.apache.marmotta.commons.sesame.repository.ResourceUtils;
import org.apache.marmotta.kiwi.model.rdf.KiWiResource;
import org.apache.marmotta.kiwi.transactions.model.TransactionData;
import org.apache.marmotta.ldpath.backend.sesame.SesameConnectionBackend;
import org.apache.marmotta.ldpath.exception.LDPathParseException;
import org.apache.marmotta.ldpath.model.fields.FieldMapping;
import org.apache.marmotta.ldpath.model.programs.Program;
import org.apache.marmotta.platform.core.api.task.Task;
import org.apache.marmotta.platform.core.api.task.TaskManagerService;
import org.apache.marmotta.platform.core.events.SystemStartupEvent;
import org.apache.marmotta.platform.core.qualifiers.event.Created;
import org.apache.marmotta.platform.core.qualifiers.event.Removed;
import org.apache.marmotta.platform.core.qualifiers.event.Updated;
import org.apache.marmotta.platform.core.qualifiers.event.transaction.AfterCommit;
import org.openrdf.model.*;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.StringReader;
import java.util.*;

import static org.apache.marmotta.commons.sesame.repository.ExceptionUtils.handleRepositoryException;
import static org.apache.marmotta.commons.sesame.repository.ResourceUtils.removeResource;


/**
 * The enhancer service manages and runs the configured LMF enhancers and provides common utility functions,
 * e.g. for re-running an enhancer or updating the enhancements of a resource.
 * <p/>
 * The enhancer service maintains a producer-consumer model for resource updates (similar to the SolrIndexingService),
 * i.e. when a transaction completes, all changed resources are added to blocking queues in the thread completing
 * the transaction, while separate enhancer threads pick up resources from the queue in turn and run the enhancement
 * engines on them.
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class EnhancerEngineServiceImpl extends WorkerServiceImpl<EnhancementRuntime, EnhancerConfiguration> implements EnhancerEngineService {


    @Inject
    private EnhancerConfigService enhancerConfigService;

    @Inject
    private EnhancerProgramService enhancerProgramService;

    /**
     * A dynamic filter for the versioning component to indicate that versions should not be created for
     * enhancement results.
     */
    @Inject
    private EnhancerStatementsFilter            versioningFilter;

    @Inject
    private TaskManagerService taskManagerService;

    @Override
    public String getName() {
        return "EnhancerEngineService";
    }

    @Override
    public EnhancementRuntime createWorker(EnhancerConfiguration config) {
        return new EnhancementRuntime(config,this);
    }

    @Override
    public List<EnhancerConfiguration> listWorkerConfigurations() {
        return enhancerConfigService.listEnhancementEngines();
    }


    @Override
    public void doBeforeReschedule(EnhancerConfiguration config) {
        if(configurationService.getBooleanConfiguration("enhancer.clean_before_reschedule",false)) {
            // remove all existing enhancements
            removeEnhancementResults(config);
        }
    }


    @Override
    public void registerWorkerRuntime(final EnhancementRuntime runtime) {
        super.registerWorkerRuntime(runtime);

        versioningFilter.addEnhancerContext(runtime.getConfiguration().getEnhancementContext());
        statementFilters.add(runtime.getConfiguration().getEnhancerFilter());
    }

    @Override
    public void removeWorkerRuntime(EnhancementRuntime runtime) {
        super.removeWorkerRuntime(runtime);

        versioningFilter.removeEnhancerContext(runtime.getConfiguration().getEnhancementContext());
        statementFilters.remove(runtime.getConfiguration().getEnhancerFilter());
    }

    /**
     * Run the enhancement engine passed as argument for the given resource, replacing all previous enhancements
     * done by this engine for this resource. The enhancement data should be connected appropriately with the
     * resource using RDF relations that can be queried.
     * <p/>
     * For example, consider enhancement of the resource R1. The resulting RDF should then be similar to the
     * following:
     * <pre>
     *     R1 enhancer:hasEnhancement E1 .
     *     E1 ...
     *     R1 enhancer:hasEnhancement E2 .
     *     E2 ...
     * </pre>
     * The property used for connecting the resource with its enhancements is up to the enhancement engine used,
     * and come e.g. from the fields in an LDPath program.
     * <p/>
     * The enhancement service will take care of properly storing the enhancement results in the
     * appropriate enhancement context of the LMF triple store, deleting previously existing enhancement results
     * for the resource. Enhancement results are considered "volatile" and will therefore not be considered for
     * versioning.
     *
     * @param engine
     * @param resource
     */
    @SuppressWarnings("unchecked")
    @Override
    public void executeEnhancementEngine(EnhancerConfiguration engine, Resource resource) {
        try {
            RepositoryConnection con = sesameService.getConnection();
            try {
                con.begin();

                executeEnhancementEngine(engine, resource, con, con);

                con.commit();
            } catch(RepositoryException ex) {
                con.rollback();
            } finally {
                con.close();
            }
        } catch(RepositoryException ex) {
            log.error("error while executing enhancement engine:", ex);
        }
    }

    /**
     * Execute the enhancement engine with the given configuration. Use the source connection to query for the data using the LDPath configuration of
     * the engine. Write the results to the target connection. The caller needs to take care of properly managing the connections. This method will neither
     * begin nor commit transactions.
     * <p/>
     * @param engine
     * @param resource
     * @param target
     * @throws RepositoryException
     */
    @Override
    public void executeEnhancementEngine(EnhancerConfiguration engine, Resource resource, RepositoryConnection source, RepositoryConnection target) throws RepositoryException {
        Program<Value> program = engine.getProgram();
        if(program == null) {
            try {
                program = enhancerProgramService.parseProgram(new StringReader(engine.getProgramString()));
                engine.setProgram(program);
            } catch (LDPathParseException e) {
                log.error("error parsing path program for engine {}",engine.getName(),e);
                return;
            }
        }

        SesameConnectionBackend backend = SesameConnectionBackend.withConnection(source);

        if (program.getFilter() != null && !program.getFilter().apply(backend, resource, Collections.singleton((Value) resource))) {
            log.debug("({}) <{}> does not match filter '{}', ignoring", engine.getName(), resource, program.getFilter().getPathExpression(backend));
            return;
        } else if (log.isTraceEnabled() && program.getFilter() != null) {
            log.trace("({}) <{}> matches filter '{}', processing...", engine.getName(), resource, program.getFilter().getPathExpression(backend));
        }

        ValueFactory valueFactory = target.getValueFactory();

        cleanEnhancements(engine, resource, target);

        URI context = valueFactory.createURI(engine.getEnhancementContext());

        for (FieldMapping<?, Value> rule : program.getFields()) {

            // the URI of the relation to add for all enhancement results
            URI p_enhancement = valueFactory.createURI(rule.getFieldName());

            // get all enhancement results from the enhancement processor configured for the rule
            Collection<?> results = rule.getValues(backend, resource, null);
            for(Object object : results) {

                // store each enhancement result in the triple store
                if(object instanceof EnhancementResult) {
                    EnhancementResult result = (EnhancementResult)object;

                    try {
                        // get a connection to the LMF repository and a connection to the enhancement results
                        RepositoryConnection enhConnection = result.getEnhancementResults().getConnection();
                        try {

                            // store all triples from the enhancement results into the enhancement context of the engine
                            target.add(enhConnection.getStatements(null, null, null, true), context);

                            // add relations from the current resource to all enhancement results according to the field name of the rule
                            for(Resource enhancement : result.getEnhancementResources()) {
                                target.add(resource, p_enhancement, enhancement, context);
                            }

                            enhConnection.commit();
                        } finally {
                            enhConnection.close();
                            result.close();
                        }

                    } catch (RepositoryException e) {
                        log.error("error while opening connection to one of the repositories");
                    }


                } else {
                    if(object == null) {
                        log.warn("result of LDPath rule for enhancer {} was not an enhancement result (was: null)", engine.getName());
                    } else {
                        log.warn("result of LDPath rule for enhancer {} was not an enhancement result (was: {})", engine.getName(), object.getClass().getName());
                    }
                }
            }
        }
    }

    /**
     * Remove all enhancement results generated by the engine given as argument. This method will remove the named
     * graph associated with the engine and all triples contained therein.
     *
     * @param engine
     */
    @Override
    public void removeEnhancementResults(EnhancerConfiguration engine) {
        Task t = taskManagerService.createTask("Database Cleaner", engine.getType());
        t.updateMessage("cleaning up old enhancements");
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {
                conn.begin();
                if (ResourceUtils.isContext(conn, engine.getEnhancementContext())) {
                    URI context = conn.getValueFactory().createURI(engine.getEnhancementContext());
                    removeResource(conn, context);
                }
                conn.commit();
            } finally {
                conn.close();
            }

        } catch(RepositoryException ex) {
            handleRepositoryException(ex,EnhancerEngineServiceImpl.class);
        }
        t.endTask();
    }

    /**
     * Helper method to clean up old enhancement results for the given resource in the enhancement graph of the given
     * engine.
     *
     * @param engine
     * @param resource
     */
    private void cleanEnhancements(EnhancerConfiguration engine, Resource resource, RepositoryConnection connection) throws RepositoryException {
        log.debug("cleanup of old enhancement results of engine {} for resource {}", engine.getName(), resource);

        // we need to clean all enhancements of the current resource that have been made by the given engine
        // for this, we recursively walk over the enhancement tree to see which triples are indirectly connected
        // with the resource and remove them in case no other resource links to them
        Set<Value> removedNodes = new HashSet<Value>();
        Set<Statement> removedTriples = new HashSet<Statement>();

        if (ResourceUtils.isContext(connection, engine.getEnhancementContext())) {
            URI context = connection.getValueFactory().createURI(engine.getEnhancementContext());
            // the resources scheduled in the next round for removal; when it is empty, the algorithm terminates
            Set<Resource> scheduledResources = new HashSet<Resource>();
            scheduledResources.add(resource);

            while(!scheduledResources.isEmpty()) {
                // start a new round of traversal, initialise the working resources from the scheduled resources
                // and create a new set for scheduling new resources during processing
                Set<Resource> workingResources = scheduledResources;
                scheduledResources = new HashSet<Resource>();

                // list of resources where we already figured out that they won't be deleted this round
                Set<Resource> ignoredThisRound = new HashSet<Resource>();

                for(Resource r : workingResources) {
                    RepositoryResult<Statement> triples = connection.getStatements(r,null,null,false,context);
                    while(triples.hasNext()) {
                        Statement triple = triples.next();

                        removedTriples.add(triple);

                        // check whether we should schedule the object of the triple for removal
                        if(triple.getObject() instanceof Literal) {
                            removedNodes.add(triple.getObject());
                        } else if (triple.getObject() instanceof BNode
                                // From here on, object is an URI
                                || triple.getObject().stringValue().startsWith(configurationService.getBaseUri() + "enhancement/")
                                || triple.getObject().stringValue().startsWith("urn:")) {
                            // simple cases: blank nodes, nodes created for previous enhancements, internal nodes of enhancement results
                            scheduledResources.add((KiWiResource) triple.getObject());
                        } else if(configurationService.getBooleanConfiguration("enhancer.clean_deep",false) &&
                                !ignoredThisRound.contains(triple.getObject()) &&
                                !removedNodes.contains(triple.getObject()) &&
                                !scheduledResources.contains(triple.getObject()) &&
                                !workingResources.contains(triple.getObject())) {
                            // TODO: maybe it is better to let a separate cleanup process perform the cleaning from time to time, we could e.g. put this in
                            // optimize

                            // list all incoming triples of the object; if they are all contained
                            // either in the removed nodes or in the scheduled resources, we can schedule the object for
                            // removal; this procedure needs to be batched, because listing all incoming links of a resource
                            // is potentially a very expensive operation with lots of results
                            boolean schedule = true;
                            RepositoryResult<Statement> batch = connection.getStatements(null,null,triple.getObject(),false,context);
                            try {
                                while(batch.hasNext() && schedule) {
                                    Statement incoming = batch.next();
                                    if(!removedNodes.contains(incoming.getSubject()) && !scheduledResources.contains(incoming.getSubject()) && !workingResources.contains(incoming.getSubject())) {
                                        schedule = false;
                                        break;
                                    }
                                }
                            } finally {
                                batch.close();
                            }

                            if(schedule) {
                                scheduledResources.add((Resource) triple.getObject());
                            } else {
                                ignoredThisRound.add((Resource) triple.getObject());
                            }

                        }
                    }
                    // add the resource to the removed resources
                    removedNodes.add(r);
                }

            }
        }

        // remove triples and nodes that were collected in the transaction
        connection.remove(removedTriples);

    }


    public void startupEvent(@Observes SystemStartupEvent e) {

    }


    /**
     * React on commit of a transaction and recompute all enhancements for the updated resources. This method will
     * simply schedule all changed resources in the scheduling queues of all enhancement engines.
     * <p/>
     * This method should take care to ignore updates that have been triggered by the enhancement process itself,
     * or otherwise it might run into an undesirable endless loop.
     *
     * @param data
     */
    @Override
    public void notifyTransactionCommit(@Observes @AfterCommit TransactionData data) {
        if (!configurationService.getBooleanConfiguration("enhancer.enabled", true)) return;

        super.notifyTransactionCommit(data);
    }

    /**
     * Notify the enhancer engine service that the given enhancement engine has been added to the system. This method
     * will trigger the creation of a queue for the engine and trigger a complete rescheduling of all resources for
     * the given engine.
     *
     * @param engine
     * @see Created
     * @see at.newmedialab.lmf.enhancer.api.config.EnhancerConfigService#addEnhancementEngine(at.newmedialab.lmf.enhancer.model.enhancer.EnhancerConfiguration)
     */
    @Override
    public void notifyEngineAdd(@Observes @Created EnhancerConfiguration engine) {
        super.notifyEngineAdd(engine);
    }

    /**
     * Notify the enhancer engine service that the given enhancement engine has been updated. This method will
     * trigger a complete rescheduling of all resources for the given engine.
     *
     * @param engine
     * @see Updated
     * @see at.newmedialab.lmf.enhancer.api.config.EnhancerConfigService#updateEnhancementEngine(at.newmedialab.lmf.enhancer.model.enhancer.EnhancerConfiguration)
     */
    @Override
    public void notifyEngineUpdate(@Observes @Updated EnhancerConfiguration engine) {
        super.notifyEngineUpdate(engine);
    }

    /**
     * Notify the enhancer engine service that the given enhancement engine has been removed from the system.
     * This method will trigger removing all enhancement results of this engine and cleanup all resources currently
     * occupied by the engine (e.g. enhancement queue).
     *
     * @param engine
     * @see Removed
     * @see at.newmedialab.lmf.enhancer.api.config.EnhancerConfigService#removeEnhancementEngine(at.newmedialab.lmf.enhancer.model.enhancer.EnhancerConfiguration)
     */
    @Override
    public void notifyEngineRemove(@Observes @Removed EnhancerConfiguration engine) {
        super.notifyEngineRemove(engine);
    }

}
