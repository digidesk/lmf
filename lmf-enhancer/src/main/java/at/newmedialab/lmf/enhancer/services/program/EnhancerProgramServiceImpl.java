/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.services.program;

import at.newmedialab.lmf.enhancer.api.program.EnhancerProgramService;
import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementProcessor;
import org.apache.marmotta.ldpath.LDPath;
import org.apache.marmotta.ldpath.backend.sesame.SesameConnectionBackend;
import org.apache.marmotta.ldpath.exception.LDPathParseException;
import org.apache.marmotta.ldpath.model.Constants;
import org.apache.marmotta.ldpath.model.programs.Program;
import org.apache.marmotta.ldpath.parser.Configuration;
import org.apache.marmotta.ldpath.parser.DefaultConfiguration;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.ldpath.api.AutoRegisteredLDPathFunction;
import org.openrdf.model.Value;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This service provides initialisation and configuration for the LDPath backend as used by the enhancement
 * configuration, and offers convenience functions for parsing LDPath programs.
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class EnhancerProgramServiceImpl implements EnhancerProgramService {

    @Inject
    private Logger log;


    @Inject
    private SesameService sesameService;

    @Inject @Any
    private Instance<AutoRegisteredLDPathFunction> functions;

    @Inject @Any
    private Instance<EnhancementProcessor> processors;


    private Configuration<Value> ldpathConfig;



    /**
     * Initialise the LDPath backend to use by the enhancer module and register the stanbol:engine transformer
     * (a normal string transformer that is interpreted by the EnhancerEngineService)
     */
    @Override
    @PostConstruct
    public void initialize() {
        log.info("LMF Enhancer Program Service starting up ...");

        ldpathConfig = new DefaultConfiguration<Value>();

        registerFunctions();
        registerTransformers();
    }

    public void registerFunctions() {
        for(AutoRegisteredLDPathFunction function : functions) {
            log.debug("Register fn:{}", function.getLocalName());
            ldpathConfig.addFunction(Constants.NS_LMF_FUNCS + function.getLocalName(), function);
        }
    }


    public void registerTransformers() {
        for(EnhancementProcessor processor : processors) {
            ldpathConfig.addNamespace(processor.getNamespacePrefix(), processor.getNamespaceUri());
            ldpathConfig.addTransformer(processor.getNamespaceUri() + processor.getLocalName(), processor);
        }
    }


    /**
     * Parse a program from the passed InputStream using the backend registered for the enhancer module.
     *
     *
     * @param program
     * @return
     * @throws LDPathParseException
     *
     */
    @Override
    public Program<Value> parseProgram(InputStream program) throws LDPathParseException {
        return parseProgram(new InputStreamReader(program));
    }

    /**
     * Parse a program frim the passed Reader using the backend registered for the enhancer module.
     *
     *
     * @param program
     * @return
     * @throws org.apache.marmotta.ldpath.exception.LDPathParseException
     *
     */
    @Override
    public Program<Value> parseProgram(Reader program) throws LDPathParseException {
        try {
            final RepositoryConnection conn = sesameService.getConnection();
            try {
                conn.begin();
                SesameConnectionBackend backend = SesameConnectionBackend.withConnection(conn);

                LDPath<Value> ldpath = new LDPath<Value>(backend, ldpathConfig);
                Program<Value> result = ldpath.parseProgram(program);

                return result;
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (RepositoryException ex) {
            throw new LDPathParseException(ex);
        }
    }

    /**
     * return a collection of all registered enhancement processors
     * @return
     */
    @Override
    public Collection<EnhancementProcessor> getProcessors() {
        Set<EnhancementProcessor> result = new HashSet<EnhancementProcessor>();
        for(EnhancementProcessor processor : processors) {
            result.add(processor);
        }
        return result;
    }
}
