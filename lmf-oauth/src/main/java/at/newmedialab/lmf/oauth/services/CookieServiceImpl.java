/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.services;

import at.newmedialab.lmf.oauth.api.CookieService;

import kiwi.core.api.config.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.NewCookie;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Stephanie Stroka
 *         User: sstroka
 *         Date: 27.09.2011
 *         Time: 10:27:42
 */
@Named("kiwi.core.cookieService")
@ApplicationScoped
public class CookieServiceImpl implements CookieService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private ConfigurationService configurationService;

    @Override
    public Cookie getCookie(HttpServletRequest request, String name) {
        Cookie[] cookies = ((HttpServletRequest) request).getCookies();
        if(cookies == null) {
            return null;
        }
        for(Cookie c : cookies) {
            String cname = c.getName();
            if(cname.equals(name)) {

                return c;
            }
        }
        return null;
    }

    @Override
    public String getCookieValue(HttpServletRequest request, String name) {
        Cookie c = getCookie(request, name);
        return c != null ? c.getValue() : null;
    }

    @Override
    public NewCookie buildCookie(String name, String value, String str_expires) {
        if(str_expires == null) {
            return buildCookie(name, value, 0);
        }
        try {
            Integer expires = new Integer(str_expires);
            return buildCookie(name, value, expires);
        } catch(NumberFormatException e) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd-MM-yyyy HH:mm:ss z");
            try {
                Date date = dateFormat.parse(str_expires);
                int expires = (int) (System.currentTimeMillis() - date.getTime());
                return buildCookie(name, value, expires);
            } catch (ParseException e1) {
                e.printStackTrace();
                e1.printStackTrace();
                return buildCookie(name, value, NewCookie.DEFAULT_MAX_AGE);
            }
        }
    }

    @Override
    public NewCookie buildCookie(String name, String value, String str_expires, boolean secure) {
        try {
            Integer expires = new Integer(str_expires);
            return buildCookie(name, value, expires, secure);
        } catch(NumberFormatException e) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd-MM-yyyy HH:mm:ss z");
            try {
                Date date = dateFormat.parse(str_expires);
                int expires = (int) (System.currentTimeMillis() - date.getTime());
                return buildCookie(name, value, expires, secure);
            } catch (ParseException e1) {
                e.printStackTrace();
                e1.printStackTrace();
                return buildCookie(name, value, NewCookie.DEFAULT_MAX_AGE, secure);
            }
        }
    }

    @Override
    public NewCookie buildCookie(String name, String value, String path, String str_expires, boolean secure) {
        try {
            Integer expires = new Integer(str_expires);
            return buildCookie(name, value, path, expires, secure);
        } catch(NumberFormatException e) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd-MM-yyyy HH:mm:ss z");
            try {
                Date date = dateFormat.parse(str_expires);
                int expires = (int) (System.currentTimeMillis() - date.getTime());
                return buildCookie(name, value, path, expires, secure);
            } catch (ParseException e1) {
                e.printStackTrace();
                e1.printStackTrace();
                return buildCookie(name, value, path, NewCookie.DEFAULT_MAX_AGE, secure);
            }
        }
    }

    @Override
    public NewCookie buildCookie(String name, String value, String path, String domain, String version, String str_expires, boolean secure) {
        try {
            Integer expires = new Integer(str_expires);
            return buildCookie(name, value, path, domain, version, expires, secure);
        } catch(NumberFormatException e) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss z");
            DateFormatSymbols symbols = new DateFormatSymbols(Locale.US);
            dateFormat.setDateFormatSymbols(symbols);
            try {
                Date date = dateFormat.parse(str_expires);
                int expires = (int) (date.getTime()-System.currentTimeMillis())/1000;
                return buildCookie(name, value, path, domain, version, expires, secure);
            } catch (ParseException e1) {
                e.printStackTrace();
                e1.printStackTrace();
                return buildCookie(name, value, path, domain, version, NewCookie.DEFAULT_MAX_AGE, secure);
            }
        }
    }

    @Override
    public NewCookie buildCookie(String name, String value, int expires) {
        return buildCookie(name, value, expires, false);
    }

    @Override
    public NewCookie buildCookie(String name, String value, int expires, boolean secure) {
        return buildCookie(name, value, configurationService.getPath(), expires, secure);
    }

    @Override
    public NewCookie buildCookie(String name, String value, String path, int expires, boolean secure) {
        return buildCookie(name, value, path, null, null, expires, secure);
    }

    @Override
    public NewCookie buildCookie(String name, String value, String path, String domain, String version, int expires, boolean secure) {
        return new NewCookie(name, value, path, domain, version, expires, false);
    }

    @Override
    public Cookie removeCookie(Cookie cookie) {
        if(cookie == null) {
            return null;
        }
        Cookie c = new Cookie(cookie.getName(), cookie.getValue());
        c.setMaxAge(0);
        return c;
    }

    @Override
    public NewCookie removeCookie(NewCookie cookie) {
        if(cookie == null) {
            return null;
        }
        return buildCookie(cookie.getName(), cookie.getValue(), 0);
    }
}
