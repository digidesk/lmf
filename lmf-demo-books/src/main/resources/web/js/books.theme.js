/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {

AjaxSolr.theme.prototype.result = function (doc, snippet) {
  
  var output = '<li><h3 class="supertitle"><p id="links_' + doc.id + '"class="metainfo"></p></h3>';
  output += '<h4 class="title"> <a href="'+doc['lmf.uri']+'">' + doc.title + '</a></h4>';
  output += '<h5>by '+doc.writer+'</h5>';
  if(doc.thumb) { 
	output += '<img class="left" src="'+doc.thumb+'"/>';
  }
  output += '<p class="preview">' + snippet + '</p>';
  var date = new Date(doc['lmf.created']);
  var yesterday = new Date();
  yesterday.setDate(yesterday.getDate()-1);
  var strDate;
  if (date.strftime('%x') == yesterday.strftime('%x')) {
    strDate = "gestern";
  } else {
    strDate = date.strftime('%x');
  }
  output += '<p class="meta">' + 'created: ' + strDate + '</p></li>';
  
  return output;
};

AjaxSolr.theme.prototype.snippet = function (doc, response) {
  var output = '';
  if (response && response.highlighting && response.highlighting[doc.id] && response.highlighting[doc.id].description) {
    var desc = response.highlighting[doc.id].description + '';
    output += desc;
  } else {
    var desc = doc.description +'';
    if (desc.length > 300) {
      output += desc.substring(0, 300) + " " + "...";
      output += '<span style="display:none;">' + desc.substring(300);
      output += '</span> <a href="#" class="more">[read more]</a>';
    } else {
      output += desc;
    }
  }
  return output;
};

AjaxSolr.theme.prototype.tag = function (facet, weight, handler) {
  var title = facet.value;
  if (title.length > 30) {
    title = title.substring(0,30) + "...";
  }
  var output  = $('<li/>', {class:(facet.active?"checked":"notchecked")}).append($('<a class="" href="#">' + title + '</a>').click(handler)).append('<span class="checkbox"></span>' + '<span class="facNum">' + facet.count + '</span>');
  return output;  
  
};

AjaxSolr.theme.prototype.facet_link = function (value, handler) {
  return $('<a href="#"/>').text(value).click(handler);
};

AjaxSolr.theme.prototype.no_items_found = function () {
  return 'no items found in current selection';
};
})(jQuery);