/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.BasicClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class HttpClientBuilder {
	
    private HttpClientBuilder() {
        // static only
    }

    public static HttpClient createHttpClient() {
        return createInternal();
    }

    private static DefaultHttpClient createInternal() {
        BasicClientConnectionManager manager = new BasicClientConnectionManager();
        BasicHttpParams params = new BasicHttpParams();
        params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);
        params.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 10000);

        final String uaString = "LMF Import Client";
        params.setParameter(CoreProtocolPNames.USER_AGENT, uaString);

        params.setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, true);
        params.setIntParameter(ClientPNames.MAX_REDIRECTS, 5);

        DefaultHttpClient http = new DefaultHttpClient(manager, params);
        return http;
    }

    public static HttpClient createHttpClient(String lmfURL, String credentials) {
        try {
            final AbstractHttpClient http = createInternal();

            final URL url = new URL(lmfURL);
            http.getParams().setBooleanParameter(ClientPNames.HANDLE_AUTHENTICATION, true);
            final BasicCredentialsProvider auth = new BasicCredentialsProvider();
            auth.setCredentials(new AuthScope(url.getHost(), url.getPort()), new UsernamePasswordCredentials(credentials));
            http.setCredentialsProvider(auth);

            return http;
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Invalid LMF_URL", e);
        }
    }

}
