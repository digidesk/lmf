/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.api;

/**
 * A base descriptor for a content item. Returns the content item's URI. Importer modules that subclass BaseImporter can
 * also extend this base class to provide additional information.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public interface BaseContentItem {

    /**
     * Return the URI of this content item (used as subject for triples).
     * @return
     */
	@Deprecated
    public String getUri(String baseUri);
    
    /**
     * Return the URI of this content item (used as subject for triples).
     * @return
     */
    public String getUri();    

 }
