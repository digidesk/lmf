/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processing Job 
 * 
 * @author Sergio Fernández
 * @author Rupert Westenthaler
 *
 */
public abstract class ProcessingJob<T> implements Runnable {

	protected Logger log = LoggerFactory.getLogger(ProcessingJob.class);
    
    private ProcessingTracker<? super T> tracker;
    private long processingTime;
    private boolean state;

    protected ProcessingJob(){}
    
    protected final void setTracker(ProcessingTracker<? super T> tracker) {
        this.tracker = tracker;
    }
    /**
     * Processes the job
     * @return
     * @throws Exception if the processing fails
     */
    
    public abstract boolean process() throws Exception;
    
    /**
     * The processed item used to keep a list of failed Items
     * @return the processed item
     */
    public abstract T getItem();
    
    final long getProcessingTime() {
        return processingTime;
    }
    
    final boolean hasSucceeded() {
        return state;
    }
    
    public final void run() {
        long start = System.currentTimeMillis();
        try {
            state = process();
            processingTime = System.currentTimeMillis()-start;
        } catch (Throwable e) {
            log.info("Processing of "+getItem()+" failed!",e);
            processingTime = System.currentTimeMillis()-start;
            state = false;         
        } finally {
            tracker.completed(this);
        }
    }
    
}