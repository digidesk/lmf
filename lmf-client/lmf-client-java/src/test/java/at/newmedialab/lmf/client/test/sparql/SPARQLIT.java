/*
 * Copyright 2012 Salzburg Research
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.newmedialab.lmf.client.test.sparql;

import at.newmedialab.lmf.client.ClientConfiguration;
import at.newmedialab.lmf.client.clients.SPARQLClient;
import at.newmedialab.lmf.client.model.sparql.SPARQLResult;
import at.newmedialab.lmf.client.test.ldpath.LDPathIT;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.importer.ImportService;
import org.apache.marmotta.platform.core.exception.io.MarmottaImportException;
import org.apache.marmotta.platform.core.test.base.JettyMarmotta;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
public class SPARQLIT {

    private static JettyMarmotta lmf;

    private static ClientConfiguration config;

    // the tests require the demo-data.foaf to be loaded; we do so by first calling the import service before we start with tests
    private static ImportService importService;

    private static ConfigurationService configurationService;


    @BeforeClass
    public static void init() throws MarmottaImportException, InterruptedException {
        lmf = new JettyMarmotta("/LMF");

        configurationService = lmf.getService(ConfigurationService.class);
        configurationService.setBooleanConfiguration("ldcache.enabled", false);

        // give the sesame service some time to recover
        Thread.sleep(1000);


        config = new ClientConfiguration("http://localhost:"+lmf.getPort()+lmf.getContext());

        importService = lmf.getService(ImportService.class);

        // load initial data
        InputStream data =  LDPathIT.class.getResourceAsStream("/demo-data.foaf");

        importService.importData(data,"application/rdf+xml",null,null);
    }

    @AfterClass
    public static void tearDown() {
        lmf.shutdown();
    }


    @Test
    public void testSparqlSelect() throws Exception {
        SPARQLClient client = new SPARQLClient(config);
        SPARQLResult result = client.select("SELECT ?r ?n WHERE { ?r <http://xmlns.com/foaf/0.1/name> ?n } ORDER BY ?n");
        Assert.assertEquals(3, result.size());
        Assert.assertTrue(result.getFieldNames().contains("r"));
        Assert.assertTrue(result.getFieldNames().contains("n"));
        Assert.assertEquals("Anna Schmidt", result.getFirst().get("n").toString());
    }


    @Test
    public void testSparqlAsk() throws Exception {
        SPARQLClient client = new SPARQLClient(config);

        boolean result = client.ask("ASK { ?r <http://xmlns.com/foaf/0.1/name> ?n }");
        Assert.assertTrue(result);

    }

}
