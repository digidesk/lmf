<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (c) 2013 Salzburg Research.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the "License");
  ~  you may not use this file except in compliance with the License.
  ~  You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing, software
  ~  distributed under the License is distributed on an "AS IS" BASIS,
  ~  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~  See the License for the specific language governing permissions and
  ~  limitations under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>at.newmedialab.lmf</groupId>
        <artifactId>lmf-parent</artifactId>
        <version>3.1.0</version>
    </parent>

    <artifactId>lmf-stanbol</artifactId>
    <packaging>jar</packaging>

    <name>LMF Module: Stanbol Integration</name>
    <description>
        Provides support for integrating the LMF with Apache Stanbol for content enhancement
        and data reconciliation. Includes an embedded version of the Apache Stanbol framework.
    </description>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-clean-plugin</artifactId>
                <version>2.5</version>
                <configuration>
                    <filesets>
                        <fileset>
                            <directory>/tmp/lmf-test</directory>
                            <followSymlinks>true</followSymlinks>
                        </fileset>
                    </filesets>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>yuicompressor-maven-plugin</artifactId>
                <version>1.0.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>aggregate</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <nomunge>true</nomunge>
                    <sourceDirectory>${project.basedir}/src/main/resources</sourceDirectory>
                    <includes>
                        <include>web/admin/js/stanbol.js</include>
                    </includes>
                    <output>${project.build.outputDirectory}/web/admin/js/stanbol.js</output>
                </configuration>
            </plugin>
       </plugins>


        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <excludes>
                    <exclude>web/admin/js/stanbol.js</exclude>
                </excludes>
                <filtering>false</filtering>
                <targetPath>${project.build.outputDirectory}</targetPath>
            </resource>
        </resources>
    </build>

    <dependencies>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-user</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-ldpath</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-ldcache</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpmime</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpcore</artifactId>
        </dependency>

        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>jquery-ui</artifactId>
        </dependency>
        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>jquery-ui-themes</artifactId>
        </dependency>


        <dependency>
            <groupId>at.newmedialab.lmf</groupId>
            <artifactId>lmf-enhancer</artifactId>
            <version>${project.version}</version>
        </dependency>

        <dependency>
            <groupId>at.newmedialab.lmf.stanbol</groupId>
            <artifactId>stanbol-embedded</artifactId>
            <version>0.14.7-LMF</version>
            <type>jar</type>
        </dependency>



        <!-- LMF Testing Package -->
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-core</artifactId>
            <version>${marmotta.version}</version>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-server</artifactId>
            <version>${jetty.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-servlet</artifactId>
            <version>${jetty.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.jboss.weld.se</groupId>
            <artifactId>weld-se-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.jayway.restassured</groupId>
            <artifactId>rest-assured</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.google.code.tempus-fugit</groupId>
            <artifactId>tempus-fugit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.googlecode.jatl</groupId>
            <artifactId>jatl</artifactId>
            <scope>test</scope>
        </dependency>

    </dependencies>
</project>
