package at.newmedialab.lmf.stanbol.test;

import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import at.newmedialab.lmf.stanbol.api.StanbolSyncService;
import com.jayway.restassured.RestAssured;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.importer.ImportService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.exception.io.MarmottaImportException;
import org.apache.marmotta.platform.core.test.base.JettyMarmotta;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
public class StanbolSyncServiceIT {

    private final static Logger log = LoggerFactory.getLogger(StanbolSyncServiceIT.class);

    private static JettyMarmotta lmf;

    private static ImportService importService;
    private static ConfigurationService configurationService;
    private static StanbolSyncService syncService;
    private static SesameService sesameService;

    @BeforeClass
    public static void init() throws MarmottaImportException {
        lmf = new JettyMarmotta("/LMF",8080);

        importService = lmf.getService(ImportService.class);
        configurationService = lmf.getService(ConfigurationService.class);
        syncService = lmf.getService(StanbolSyncService.class);
        sesameService = lmf.getService(SesameService.class);


        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
        RestAssured.basePath = "/LMF";

    }

    @AfterClass
    public static void tearDown() {
        lmf.shutdown();
    }

    @Ignore
    @Test
    public void testPublishContext() throws MarmottaImportException, InterruptedException, RepositoryException {
        // LMF must be ready
        expect().
        statusCode(200).
        when().
        get("/config/list");


        // Stanbol must be ready
        given().
        header("Accept","application/json").
        expect().
        statusCode(200).
        when().
        get("/stanbol/config/entityhub/sites/referenced");


        String uri = "http://localhost:8080/context/persons";

        RepositoryConnection conn = sesameService.getConnection();
        URI ctx = conn.getValueFactory().createURI(uri);
        conn.commit();

        // load initial data
        InputStream data =  StanbolSyncServiceIT.class.getResourceAsStream("Persons.rdf");

        importService.importData(data,"application/rdf+xml",null,ctx);


        // publish core to stanbol
        List<String> enabledContexts = new ArrayList<String>(configurationService.getListConfiguration("stanbol.published_contexts"));
        enabledContexts.add(uri);
        configurationService.setListConfiguration("stanbol.published_contexts",enabledContexts);

        // test whether stanbol has data (after waiting for some time)
        Thread.sleep(1000);

        // the site should exist and return code 200
        expect().
        statusCode(200).
        when().
        get("/stanbol/config/entityhub/site/"+syncService.getSiteName(ctx)+"/");


        // when querying the site for a given resource, it should return the resource in JSON-LD
        String rupert = "http://localhost:8080/LMF/resource/CHTpgeDx";
        expect().
        statusCode(200).
        body("representation.id", equalTo(rupert)).
        when().
        get("/stanbol/config/entityhub/site/"+syncService.getSiteName(ctx)+"/entity?id="+rupert);
    }

}
