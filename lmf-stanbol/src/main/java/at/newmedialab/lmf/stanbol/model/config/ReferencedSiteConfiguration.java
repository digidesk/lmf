/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.config;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Configuration for a referenced site, indicating where to locate the index file, where to locate the
 * bundle jar, a name, and a short description.
 * <p/>
 * Author: Sebastian Schaffert
 */
public class ReferencedSiteConfiguration {

    private String id;

    private String name;

    private String description;

    private URI bundleLocation;

    private URI indexLocation;

    private long size;


    public ReferencedSiteConfiguration(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public ReferencedSiteConfiguration(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public ReferencedSiteConfiguration(String id, String name, String description, String bundleLocation, String indexLocation, long size) throws URISyntaxException {
        this.id = id;
        this.name = name;
        this.description = description;

        this.bundleLocation = new URI(bundleLocation);
        this.indexLocation  = new URI(indexLocation);
        this.size = size;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public URI getBundleLocation() {
        return bundleLocation;
    }

    public void setBundleLocation(URI bundleLocation) {
        this.bundleLocation = bundleLocation;
    }

    public URI getIndexLocation() {
        return indexLocation;
    }

    public void setIndexLocation(URI indexLocation) {
        this.indexLocation = indexLocation;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReferencedSiteConfiguration that = (ReferencedSiteConfiguration) o;

        if (size != that.size) return false;
        if (bundleLocation != null ? !bundleLocation.equals(that.bundleLocation) : that.bundleLocation != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (indexLocation != null ? !indexLocation.equals(that.indexLocation) : that.indexLocation != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (bundleLocation != null ? bundleLocation.hashCode() : 0);
        result = 31 * result + (indexLocation != null ? indexLocation.hashCode() : 0);
        result = 31 * result + (int) (size ^ (size >>> 32));
        return result;
    }
}
