/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.enhancer;

import java.net.URI;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Representation of a Stanbol text enhancement result.
 * <p/>
 * Author: Sebastian Schaffert
 */
public class Enhancement {

    /** starting position of the selected text inside the analysed text */
    private int startPosition;

    /** end position of the selected text inside the analysed text */
    private int endPosition;

    /** string representation of the selected text */
    private String selectedText;

    /** confidence about the selection */
    private double confidence;

    /**
     * a URI identifying the type of entity recognised for the selected text
     */
    private URI type;

    /**
     * an ordered list of Linked Data resources that might be associated with the selected text,
     * sorted by probability
     */
    private SortedSet<EntityReference> entities;


    public Enhancement(String selectedText, int startPosition, int endPosition, double confidence, URI type) {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.selectedText = selectedText;
        this.confidence = confidence;
        this.type = type;
        this.entities = new TreeSet<EntityReference>();
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }

    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }

    public String getSelectedText() {
        return selectedText;
    }

    public void setSelectedText(String selectedText) {
        this.selectedText = selectedText;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public URI getType() {
        return type;
    }

    public void setType(URI type) {
        this.type = type;
    }


    public void addEntity(EntityReference e) {
        entities.add(e);
    }

    public SortedSet<EntityReference> getEntities() {
        return entities;
    }

}
