package at.newmedialab.lmf.stanbol.api;

import org.apache.stanbol.enhancer.topic.api.ClassifierException;
import org.apache.stanbol.enhancer.topic.api.TopicClassifier;
import org.apache.stanbol.enhancer.topic.api.TopicSuggestion;
import org.apache.stanbol.enhancer.topic.api.training.TrainingSet;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import java.util.List;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public interface TopicClassifierService {


    /**
     * Create a topic classifier using the given name and synch'ed with the given context. This method will
     * automatically create a topic classifier and a training set, and add configuration entries into the
     * system configuration to allow automated synch'ing of vocabulary updates.
     * @param context
     */
    public void createTopicClassifier(URI context) throws ClassifierException, RepositoryException;


    /**
     * Remove the topic classifier using the given name and synch'ed with the given context. This method will
     * automatically create a topic classifier and a training set, and add configuration entries into the
     * system configuration to allow automated synch'ing of vocabulary updates.
     * @param context
     */
    public void removeTopicClassifier(URI context) throws ClassifierException, RepositoryException;


    /**
     * Return the names of all topic classifiers configured in the system.
     * @return
     */
    public List<String> getTopicClassifiers();


    /**
     * Return the training set with the name given as argument
     * @param name
     * @return
     */
    public TrainingSet getTrainingSet(String name);


    /**
     * Return the training set for the vocabulary with the given context.
     * @param context
     * @return
     */
    public TrainingSet getTrainingSet(Resource context);


    /**
     * Return the topic classifier with the given name as argument.
     * @param name
     * @return
     */
    public TopicClassifier getTopicClassifier(String name);

    /**
     * Return the topic classifier for the vocabulary with the given context..
     * @param context
     * @return
     */
    public TopicClassifier getTopicClassifier(Resource context);


    /**
     * Suggest topics for the text passed as argument using the specified classifier
     * @param classifier
     * @param text
     * @return
     */
    public List<TopicSuggestion> suggestTopics(String classifier, String text) throws ClassifierException;

    /**
     * Suggest topics for the text passed as argument using the specified classifier
     * @param vocabulary
     * @param text
     * @return
     */
    public List<TopicSuggestion> suggestTopics(Resource vocabulary, String text) throws ClassifierException;


    /**
     * Add all concepts from the SKOS vocabulary available through the given repository connection. This method
     * can also be used with an InterceptingRepositoryConnection to filter on the Marmotta repository
     * @param connection
     */
    public void addSkosVocabulary(String classifier, RepositoryConnection connection) throws ClassifierException;


    /**
     * Add all concepts from the SKOS vocabulary available in the given Sesame context in the underlying Marmotta
     * repository.
     *
     * @param context
     */
    public void addSkosVocabulary(String classifier, Resource context) throws RepositoryException, ClassifierException;

}
