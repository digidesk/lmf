package at.newmedialab.lmf.stanbol.provider;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.ArrayUtils;
import org.apache.marmotta.ldclient.api.endpoint.Endpoint;
import org.apache.stanbol.entityhub.servicesapi.site.Site;

import java.util.regex.Pattern;

/**
 * A specialised LDCLient endpoint definition that allows API access to a Stanbol Site.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class StanbolSiteEndpoint extends Endpoint {

    /**
     * The site this endpoint definition is referring to.
     */
    private Site site;

    public StanbolSiteEndpoint(String name, Site site) {
        super(name, StanbolSiteProvider.STANBOL_ENTITYHUB, null, null, 86400L);
        this.site = site;

        Preconditions.checkState(site.supportsLocalMode());
        Preconditions.checkState(ArrayUtils.nullToEmpty(site.getConfiguration().getEntityPrefixes()).length > 0);

        // build the URI pattern
        StringBuilder patternBuilder = new StringBuilder();

        String[] prefixes = site.getConfiguration().getEntityPrefixes();
        patternBuilder.append("(");
        for(int i=0; i<prefixes.length; i++) {
            patternBuilder.append("^");
            patternBuilder.append(Pattern.quote(prefixes[i]));
            patternBuilder.append(".*");

            if(i+1 < prefixes.length) {
                patternBuilder.append("|");
            }
        }
        patternBuilder.append(")");
        setUriPattern(patternBuilder.toString());

        setPriority(PRIORITY_HIGH);
    }


    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        StanbolSiteEndpoint that = (StanbolSiteEndpoint) o;

        if (!site.equals(that.site)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + site.hashCode();
        return result;
    }
}
