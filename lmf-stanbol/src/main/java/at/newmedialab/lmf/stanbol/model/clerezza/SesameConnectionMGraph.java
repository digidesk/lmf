/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.clerezza;

import org.apache.clerezza.rdf.core.MGraph;
import org.openrdf.repository.RepositoryConnection;

import java.util.concurrent.locks.ReentrantLock;

/**
 * A Clerezza-Sesame MGraph wrapping a Sesame RepositoryConnection. The RepositoryConnection must be managed outside
 * the MGraph.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class SesameConnectionMGraph extends AbstractSesameMGraph implements MGraph {

    /**
     * Since the graph has a single shared connection, we use locking to avoid concurrent access to the connection
     */
    private ReentrantLock lock = new ReentrantLock();


    /**
     * The repository connection wrapped by this MGraph; must be managed by the caller.
     */
    private RepositoryConnection connection;



    /**
     * Construct an MGraph wrapping the repository connection passed as argument.
     *
     * @param connection
     */
    public SesameConnectionMGraph(RepositoryConnection connection) {
        super(connection.getValueFactory(), null);
        this.connection = connection;
    }

    /**
     * Construct an MGraph wrapping the repository connection passed as argument and filtering always on the context
     * given as second argument.
     *
     * @param connection the wrapped RepositoryConnection
     * @param context    the context used for filtering
     */
    public SesameConnectionMGraph(RepositoryConnection connection, org.openrdf.model.Resource context) {
        super(connection.getValueFactory(), context);
        this.connection = connection;
    }

    /**
     * Aqcuire the repository connection used by this repository, possibly creating a lock to avoid
     * concurrent access to the connection.
     *
     * @return
     */
    @Override
    protected RepositoryConnection aqcuireConnection() {
        lock.lock();
        return connection;
    }

    /**
     * Release the repository connection used by this repository, possibly releasing resources
     * occupied by the connection.
     *
     * @return
     */
    @Override
    protected void releaseConnection(RepositoryConnection connection) {
        lock.unlock();
    }
}
