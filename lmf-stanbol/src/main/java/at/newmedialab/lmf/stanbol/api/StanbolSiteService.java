/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.api;

import at.newmedialab.lmf.stanbol.model.config.ReferencedSiteConfiguration;
import at.newmedialab.lmf.stanbol.model.entityhub.StanbolSiteDescription;
import org.apache.stanbol.entityhub.servicesapi.site.Site;

import java.io.IOException;
import java.util.List;

/**
 * An LMF service for accessing the Stanbol Entity Hub, e.g. for entity lookup
 * <p/>
 * Author: Sebastian Schaffert
 */
public interface StanbolSiteService {

    /**
     * Return a list of the URIs of stanbol referenced sites that are available in the local stanbol entityhub
     *
     * @deprecated use listStanbolSiteUris() instead
     * @return
     */
    public List<String> listStanbolSites();

    /**
     * Return a list of the URIs of stanbol referenced sites that are available in the local stanbol entityhub
     *
     * @return
     */
    public List<String> listStanbolSiteUris();


    /**
     * Return a list of the IDs of stanbol referenced sites that are available in the local stanbol entityhub
     * @return
     */
    public List<String> listStanbolSiteIds();

    /**
     * Return a reference to the stanbol site with the given name, or null if it does not exist
     * @param name
     * @return
     */
    public Site getStanbolSite(String name);


    /**
     * Return a list of the stanbol referenced sites that can be installed in the local stanbol entityhub.
     * @return
     */
    public List<ReferencedSiteConfiguration> listInstallableSites();


    /**
     * Return a site description of the stanbol site with the URI passed as argument
     *
     * @param uri
     * @param id
     * @return
     */
    public StanbolSiteDescription getSiteDescription(String uri, String id) throws IOException;


    void registerCacheEndpoint(String siteId);
}
