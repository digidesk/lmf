/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.processor;

import at.newmedialab.lmf.enhancer.model.io.InputStreamValue;
import at.newmedialab.lmf.enhancer.model.io.TypedInputStream;
import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementProcessor;
import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementResult;
import at.newmedialab.lmf.stanbol.api.StanbolEnhancerService;
import org.apache.commons.io.IOUtils;
import org.apache.marmotta.ldpath.api.backend.RDFBackend;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * An enhancement processor that calls Apache Stanbol enhancement chains for all nodes selected by
 * <p/>
 * Author: Sebastian Schaffert (sschaffert@apache.org)
 */
@ApplicationScoped
public class StanbolProcessor implements EnhancementProcessor {

    private static final String[] SPARQL_NODEBUG = {
            "nodebugEntity.sparql",
            "nodebugSentiment.sparql"
    };

    /**
     * The namespace used by the Stanbol enhancer to identify enhancement properties
     */
    public static final String NS_FISE = "http://fise.iks-project.eu/ontology/";

    /**
     * The LMF namespace used to identify stanbol functions, transformers, properties
     */
    public static final String NS_STANBOL = "http://www.newmedialab.at/lmf/stanbol/";

    /**
     * The supertype of all Stanbol enhancements
     */
    public static final String URI_ENHANCEMENT = NS_FISE + "Enhancement";


    @Inject
    private Logger log;

    @Inject
    private StanbolEnhancerService stanbolEnhancerService;

    /**
     * Return the transformer name used for registering this result transformer.
     * <p/>
     * Example: "engine"
     * <p/>
     *
     * @return the name of the transformer as it will be used in LDPath programs
     */
    @Override
    public String getLocalName() {
        return "engine";
    }

    /**
     * Return the namespace URI used for registering this result transformer.
     * <p/>
     * Example: "http://www.newmedialab.at/lmf/stanbol/"
     * <p/>
     *
     * @return the namespace URI of the transformer
     */
    @Override
    public String getNamespaceUri() {
        return NS_STANBOL;
    }

    /**
     * Return the namespace prefix used for registering this result transformer as it will
     * be used in LDPath programs.
     * <p/>
     * Example: "stanbol"
     * <p/>
     *
     * @return the namespace prefix used for registering the transformer
     */
    @Override
    public String getNamespacePrefix() {
        return "stanbol";
    }

    /**
     * Run an Apache Stanbol enhancement chain on the RDF value passed as argument and return the enhancement results.
     *
     * @param value
     * @return
     */
    @Override
    public EnhancementResult transform(RDFBackend<Value> backend, Value value, Map<String,String> configuration) throws IllegalArgumentException {
        try {
            if(configuration == null || !configuration.containsKey("name")) {
                throw new IllegalArgumentException("no engine name was given in the configuration");
            }

            String chainName = configuration.get("name");

            // get an input stream for the RDF value we are currently processing
            TypedInputStream stream;
            if(value instanceof InputStreamValue) {
                stream = (InputStreamValue) value;
            } else {
                stream = new TypedInputStream(new ByteArrayInputStream(backend.stringValue(value).getBytes()), "text/plain");
            }

            // send enhancement stream to Apache Stanbol and retrieve the results in a separate repository
            Repository stanbolResults = stanbolEnhancerService.getEnhancementsRDF(stream.getStream(),stream.getMimeType(),chainName);


            if(stanbolResults != null) {
                Repository enhancementResults = new SailRepository(new MemoryStore());
                enhancementResults.initialize();

                RepositoryConnection enhancementsConnection = enhancementResults.getConnection();
                RepositoryConnection stanbolConnection = stanbolResults.getConnection();

                if("false".equalsIgnoreCase(configuration.get("debug"))) {
                    for(String fileName : SPARQL_NODEBUG) {
                        try {
                            String query = IOUtils.toString(this.getClass().getResourceAsStream(fileName));
                            GraphQuery q = stanbolConnection.prepareGraphQuery(QueryLanguage.SPARQL, query);
                            enhancementsConnection.add(q.evaluate());
                        } catch (IOException ex) {
                            log.warn("SPARQL filter query for Stanbol result not found or unreadable: {}", fileName);
                        } catch (MalformedQueryException ex) {
                            log.warn("SPARQL filter query for Stanbol result was invalid: {} ({})", fileName, ex.getMessage());
                        } catch (QueryEvaluationException ex) {
                            log.warn("SPARQL filter query for Stanbol result could not be evaluated: {} ({})", fileName, ex.getMessage());
                        }
                    }
                } else if(configuration.containsKey("filter")) {
                    String query = configuration.get("filter");
                    try {
                        GraphQuery q = stanbolConnection.prepareGraphQuery(QueryLanguage.SPARQL, query);
                        enhancementsConnection.add(q.evaluate());
                    } catch (MalformedQueryException ex) {
                        log.warn("SPARQL filter query for Stanbol result was invalid: {}", ex.getMessage());
                    } catch (QueryEvaluationException ex) {
                        log.warn("SPARQL filter query for Stanbol result could not be evaluated: {}", ex.getMessage());
                    }
                } else {
                    enhancementsConnection.add(stanbolConnection.getStatements(null,null,null,true));
                }

                enhancementsConnection.commit();
                enhancementsConnection.close();

                stanbolConnection.commit();
                stanbolConnection.close();
                stanbolResults.shutDown();


                // identify all enhancement result resources returned by Stanbol
                Set<Resource> enhancementResources = new HashSet<Resource>();
                try {
                    RepositoryConnection enhConnection = enhancementResults.getConnection();
                    try {
                        RepositoryResult<Statement> result =
                                enhConnection.getStatements(
                                        null,
                                        RDF.TYPE,
                                        enhancementResults.getValueFactory().createURI(URI_ENHANCEMENT), true);

                        while(result.hasNext()) {
                            enhancementResources.add(result.next().getSubject());
                        }
                    } finally {
                        enhConnection.close();
                    }
                } catch (RepositoryException e) {
                    log.error("error while accessing enhancement results repository",e);
                }

                return new EnhancementResult(enhancementResources,enhancementResults);
            } else {
                return null;
            }
        } catch(Exception ex) {
            log.error("stanbol enhancement process failed; exception follows", ex);
            return null;
        }
    }
}
