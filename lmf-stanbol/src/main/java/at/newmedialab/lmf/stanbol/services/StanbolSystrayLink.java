/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.services;

import at.newmedialab.lmf.stanbol.api.StanbolService;
import org.apache.marmotta.platform.core.api.ui.MarmottaSystrayLink;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class StanbolSystrayLink implements MarmottaSystrayLink {

    @Inject
    private StanbolService stanbolService;

    /**
     * Get the label of the systray entry for displaying in the menu.
     *
     * @return
     */
    @Override
    public String getLabel() {
        return "Apache Stanbol";
    }

    /**
     * Get the link to point the browser to when the user selects the menu entry.
     *
     * @return
     */
    @Override
    public String getLink() {
        return stanbolService.getStanbolUri();
    }

    /**
     * Get the section where to add the systray link in the menu (currently either ADMIN or DEMO).
     *
     * @return
     */
    @Override
    public Section getSection() {
        return Section.ADMIN;
    }
}
