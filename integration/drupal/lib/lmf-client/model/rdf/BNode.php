<?php
namespace LMFClient\Model\RDF;

require_once 'RDFNode.php';
use LMFClient\Model\RDF\RDFNode;

/**
 * Representation of an RDF blank node / anonymous node in PHP
 *
 * User: sschaffe
 * Date: 25.01.12
 * Time: 10:20
 * To change this template use File | Settings | File Templates.
 */
class BNode extends RDFNode
{
    /** @var the ID of the anonymous node (depending on server implementation) */
    private $anonId;

    function __construct($anonId)
    {
        $this->anonId = $anonId;
    }



    function __toString()
    {
        return "_:"+$this->anonId;
    }

    /**
     * @return \the
     */
    public function getAnonId()
    {
        return $this->anonId;
    }


}
