<?php
/**
 * Created by IntelliJ IDEA.
 * User: sschaffe
 * Date: 30.01.12
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 */
require_once 'autoload.php';

use LMFClient\ClientConfiguration;
use LMFClient\Clients\LDPathClient;

$config = new ClientConfiguration("http://localhost:8080/LMF");

$client = new LDPathClient($config);

// list friends of hans meier
foreach($client->evaluatePath("https://www.youtube.com/profile?user=wastl76", "(<http://gdata.youtube.com/schemas/2007#playlist> | <http://gdata.youtube.com/schemas/2007#favorite> ) / <http://gdata.youtube.com/schemas/2007#video> / <http://www.holygoat.co.uk/owl/redwood/0.1/tags/taggedWithTag> / <http://www.holygoat.co.uk/owl/redwood/0.1/tags/name>") as $field) {
    echo "Tag: $field\n";
}

?>