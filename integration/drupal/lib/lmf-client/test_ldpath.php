<?php
/**
 * Created by IntelliJ IDEA.
 * User: sschaffe
 * Date: 27.01.12
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
require_once 'autoload.php';

use LMFClient\ClientConfiguration;
use LMFClient\Clients\LDPathClient;

$config = new ClientConfiguration("http://localhost:8080/LMF");

$client = new LDPathClient($config);

// list friends of hans meier
foreach($client->evaluatePath("http://localhost:8080/LMF/resource/hans_meier", "foaf:knows / foaf:name") as $friend) {
    echo "Friend: $friend\n";
}

// list profile of hans meier
foreach($client->evaluateProgram("http://localhost:8080/LMF/resource/hans_meier", "friend = foaf:knows / foaf:name :: xsd:string; name = foaf:name :: xsd:string; interest   = foaf:interest / (rdfs:label[@en] | rdfs:label[@none] | <http://rdf.freebase.com/ns/type.object.name>[@en]) :: xsd:string;") as $field => $values) {
    echo "$field: ";
    foreach($values as $value) {
        echo $value . ",";
    }
    echo "\n";
}


?>
