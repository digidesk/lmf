
function StanbolReconciliationConfiguration() {}

StanbolReconciliationConfiguration.prototype.show = function() {

    var self = this; 

    var dialog = $(DOM.loadHTML("lmf","html/stanbol-reconciliation-configuration.html"));

    var inputUri = dialog.find("input#stanbol-uri");
    
    $.get("command/lmf/inspect-stanbol",
		    {
                "engine": JSON.stringify(ui.browsingEngine.getJSON()),
                "project": theProject.id
		    },
		    function(data) {
		    	self.setInput(inputUri, data.stanbol);                
            },
            "json");
    

    dialog.find("button#cancel").click(function() {
        DialogSystem.dismissUntil(self._level - 1);
    });

    dialog.find("button#register").click(function() {
    	
    	if ($("img#validation-img").length) { $("img#validation-img").remove(); }
    	
	    var uri = inputUri.val();
	    inputUri.val(normalizeUri(uri));
    	
    	if (validateURI(uri)) {
    		inputUri.attr("disabled", "disabled");
    		inputUri.after($('<img src="extension/lmf/images/spinner.gif" width="14" height="14" alt="fetching..." class="validation" id="validation-img" />'));
		    $.post("command/lmf/inspect-stanbol",
				    {
					    "uri": uri,
	                    "engine": JSON.stringify(ui.browsingEngine.getJSON()),
	                    "project": theProject.id
				    },
				    function(data) {
				    	
				    	var registering = $("dl#stanbol-registering");
				    	registering.parent().height($("p#stanbol-help").height());
				    	registering.parent().fadeIn("slow");
				    	$("p#stanbol-help").hide();
				    	$.each(data, function(i, obj) {
				    		//check issue #579: http://code.google.com/p/google-refine/issues/detail?id=579
				    		if (ReconciliationManager.getServiceFromUrl(obj.uri)) {
				    			self.printAddedService(registering, obj, false)
				    		} else {
					    	    ReconciliationManager.registerStandardService(obj.uri, function(index) {
					    	    	self.printAddedService(registering, obj, true)
					    	    });	
				    		}
				    	});
				    	$("img#validation-img").remove();
				    	//DialogSystem.dismissUntil(self._level - 1);
				    	dialog.find("button#register").hide();
				    	dialog.find("button#cancel").text("Close");
		            },
	                "json");
    	} else {
    		inputUri.addClass("error");
    		inputUri.after($('<img src="extension/lmf/images/no.png" width="16" height="16" alt="invalid" class="validation" id="validation-img" />'));	
    		alert("Not valid URI")
    	}
    });
	
	var frame = DialogSystem.createDialog();
    frame.width("500px");
    dialog.appendTo(frame);
    
	self._level = DialogSystem.showDialog(frame);
	
};

StanbolReconciliationConfiguration.prototype.printAddedService = function(container, obj, registered) {
	var cached = (obj.local ? "locally cached" : "not locally cached");
	var image = (registered ? "yes" : "no");
	var label = (registered ? "registered" : "not added (service already registered)");
	var sniper = '<dt><a href="' + obj.uri + '">' + obj.uri + '</a> <img src="../extension/lmf/images/' + image + '.png" width="16" height="16" alt="' + label + '" title="' + label + '" /></dt><dd><strong>' + obj.name + '</strong>, ' + cached + '</dd>';
	if (!registered) {
		sniper += '<dd>' + label + '</dd>';
	}
	container.append(sniper).fadeIn("slow");
}

StanbolReconciliationConfiguration.prototype.setInput = function(input, uri) {
	input.empty().val(normalizeUri(uri));
}

