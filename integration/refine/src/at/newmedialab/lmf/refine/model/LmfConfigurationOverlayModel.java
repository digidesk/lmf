package at.newmedialab.lmf.refine.model;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.newmedialab.lmf.refine.LmfConfiguration;
import at.newmedialab.lmf.refine.utils.HttpUtils;

import com.google.refine.model.OverlayModel;
import com.google.refine.model.Project;

/**
 * LMF Configuration Overlay Model
 * 
 * @author Sergio Fernández
 *
 */
public class LmfConfigurationOverlayModel implements OverlayModel {
	
	private static Logger log = LoggerFactory.getLogger(LmfConfigurationOverlayModel.class);
	private static final String KIWI_HOST = "kiwi.host";
	public final static String NAME = "lmf";
	public final static String KEYS_PREFIX = "refine.lmf.";
	private static final String URI = "uri";
	private static final String USER = "user";
	private static final String PASS = "pass";
	private static final String CONTEXT = "context";
	private static final String RESET = "reset";
	private String uri;
	private String user;
	private String pass;
	private String context;
	private boolean reset;

	public LmfConfigurationOverlayModel() {
		this(LmfConfiguration.get(KEYS_PREFIX + URI));	
	}
	
	public LmfConfigurationOverlayModel(String uri) {
		this(uri, "");
	}
	
	public LmfConfigurationOverlayModel(String uri, String ctx) {		
		this(uri, LmfConfiguration.get(KEYS_PREFIX + USER),  LmfConfiguration.get(KEYS_PREFIX + PASS), 
				ctx, LmfConfiguration.getBoolean(KEYS_PREFIX + RESET));	
	}
	
	public LmfConfigurationOverlayModel(HttpServletRequest req) {
		this(retrieveActualHost(req, LmfConfiguration.get(KEYS_PREFIX + URI)));	
	}
		
	public LmfConfigurationOverlayModel(String uri, String user, String pass, String ctx, boolean reset) {	
		super();
		this.uri = uri;
		this.user = user;		
		this.pass = pass;
		this.context = ctx;
		this.reset = reset;	
	}

	private static String retrieveActualHost(HttpServletRequest req, String defaultUri) {
		String resource = "http://" + req.getServerName() + ":8080/LMF/config/data/" + KIWI_HOST;
		try {
			HttpEntity entity = HttpUtils.get(resource, "application/json");
			JSONObject json = new JSONObject(EntityUtils.toString(entity));
			if (json.has(KIWI_HOST)) {
				String actualHost = json.getString(KIWI_HOST);
				if (actualHost.charAt(actualHost.length()-1) == '/') {
					actualHost = actualHost.substring(0, actualHost.length()-1);
				}
				log.info("Retrieved the actual host from LMF: " + actualHost);
				return actualHost;
			} else {
				log.error("LMF host is missing, so returning the default one");
				return defaultUri;
			}
		} catch (IOException e) {
			log.error("Problems requesting " + resource + ": " + e.getMessage());
			return defaultUri;
		} catch (JSONException e) {
			log.error("Problems parsing json returned by " + resource + ": " + e.getMessage());
			return defaultUri;
		}
	}

	@Override
	public void write(JSONWriter writer, Properties options) throws JSONException {
		writer.object();
		writer.key(URI); 
		writer.value(this.uri);
		writer.key(USER);
		writer.value(this.user);
		writer.key(PASS);
		writer.value(this.pass);     
		writer.key(CONTEXT);
		writer.value(this.context); 
		writer.key(RESET);
		writer.value(this.reset); 
		writer.endObject();
	}
	
	static public OverlayModel reconstruct(JSONObject json) throws JSONException {
		LmfConfigurationOverlayModel overlay = new LmfConfigurationOverlayModel();
		overlay.setUri(json.getString(URI));
		overlay.setUser(json.getString(USER));
		overlay.setPass(json.getString(PASS));
		overlay.setContext(json.getString(CONTEXT));
		overlay.setReset(json.getBoolean(RESET));
		return overlay;
	}

	@Override
	public void onBeforeSave(Project project) {
		
	}

	@Override
	public void onAfterSave(Project project) {
		
	}

	@Override
	public void dispose(Project project) {
		
	}

	public String getUri() {
		return this.uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public boolean isReset() {
		return reset;
	}

	public void setReset(boolean reset) {
		this.reset = reset;
	}

	@Override
	public String toString() {
		return "LmfConfigurationOverlayModel [uri=" + uri + ", user=" + user
				+ ", pass=" + pass + ", context=" + context + ", reset="
				+ reset + "]";
	}

}
